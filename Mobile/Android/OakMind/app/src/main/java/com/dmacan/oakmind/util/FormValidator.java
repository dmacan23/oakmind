package com.dmacan.oakmind.util;

import android.widget.EditText;

/**
 * Created by David on 17.8.2014..
 */
public class FormValidator {

    public static boolean isEmpty(EditText et) {
        return (et.getText().toString().matches(Const.Values.EMPTY));
    }

    public static boolean isPasswordMatch(String password, String passwordConfirm) {
        return password.equals(passwordConfirm);
    }

    public static boolean isUsernameValid(String username) {
        return username.matches(Const.Regex.ALPHANUMERIC);
    }

    public static boolean isEmailValid(String email) {
        return true;
        //return email.matches(Const.Regex.EMAIL);
    }
}
