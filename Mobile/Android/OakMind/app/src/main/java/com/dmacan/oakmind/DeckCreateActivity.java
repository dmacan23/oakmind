package com.dmacan.oakmind;

import android.os.Bundle;

import com.dmacan.oakmind.data.Deck;
import com.dmacan.oakmind.fragment.DeckCreateFragment;
import com.dmacan.oakmind.util.APIUtil;
import com.dmacan.oakmind.util.Const;

/**
 * Created by David on 23.8.2014..
 */
public class DeckCreateActivity extends OakMindActivity {

    private Deck deck;

    @Override
    public int provideLayoutRes() {
        return R.layout.activity_container;
    }

    @Override
    public void main(Bundle savedInstanceState) {
        Bundle extras = getIntent().getExtras();
        if (extras != null)
            deck = APIUtil.createGson().fromJson(extras.getString(Const.Key.DECK_CURRENT), Deck.class);
        setupFragment(R.id.container, new DeckCreateFragment());
    }

    public Deck getDeck() {
        return deck;
    }

    public void setDeck(Deck deck) {
        this.deck = deck;
    }
}
