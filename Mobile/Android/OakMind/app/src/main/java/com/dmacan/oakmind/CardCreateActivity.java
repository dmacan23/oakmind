package com.dmacan.oakmind;

import android.os.Bundle;

import com.dmacan.oakmind.fragment.CardCreateFragment;

/**
 * Created by David on 23.8.2014..
 */
public class CardCreateActivity extends OakMindActivity {
    @Override
    public int provideLayoutRes() {
        return R.layout.activity_container;
    }

    @Override
    public void main(Bundle savedInstanceState) {
        setupFragment(R.id.container, new CardCreateFragment());
    }
}
