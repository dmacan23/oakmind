package com.dmacan.oakmind.core;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by David on 31.8.2014..
 */
public class Leitner {

    public static final int MODEL_SIMPLE = 2;
    public static final int MODEL_ADVANCED = 5;
    private static final String TAG = "Leitner";
    private static final int START_DECK = 1;

    private static int currentDeck = 1;

    private ExamDeck[] examDecks;
    private ExamDeck startDeck;
    private ExamDeck finalDeck;
    private int model;

    public static int getCurrentDeck() {
        return currentDeck;
    }

    private static void log(Object message) {
        Log.d(TAG, message.toString());
    }

    public void startExam(ExamDeck deck, int model) {
        this.startDeck = deck;
        prepareExam(model);
    }

    public void startExam(List<ExamCard> cards, int model) {
        this.startDeck = new ExamDeck(cards);
        prepareExam(model);
    }

    public void prepareExam(int model) {
        currentDeck = 1;
        this.model = model;
        examDecks = new ExamDeck[model];
        initExamDecks();
    }

    private void initExamDecks() {
        finalDeck = new ExamDeck();
        for (int i = 0; i < examDecks.length; i++)
            examDecks[i] = new ExamDeck();
        examDecks[START_DECK].setCards(new ArrayList<ExamCard>(startDeck.getCards()));
        examDecks[START_DECK].shuffle();
    }

    public void answerCorrect(ExamCard card) {
        if (!isExamCompleted()) {
            shiftCardUp(card);
            selectCurrentDeck();
        }
    }

    public void answerIncorrect(ExamCard card) {
        if (!isExamCompleted()) {
            shiftCardToStart(card);
            selectCurrentDeck();
        }
    }

    public void shiftCardUp(ExamCard card) {
        examDecks[currentDeck].removeCard(card);
        if (currentDeck < examDecks.length - 1)
            examDecks[currentDeck + 1].addCard(card);
        else
            finalDeck.addCard(card);
    }

    public void shiftCardToStart(ExamCard card) {
        examDecks[currentDeck].removeCard(card);
        examDecks[0].addCard(card);
    }

    public boolean isCurrentDeckEmpty() {
        return examDecks[currentDeck].getCards().isEmpty();
    }

    public boolean isExamCompleted() {
        return (finalDeck.getCards().size() == startDeck.getCards().size());
    }

    public ExamCard drawCard() {
        if (!isExamCompleted())
            return examDecks[currentDeck].drawLastCard();
        return null;
    }

    public void selectCurrentDeck() {
        if (isCurrentDeckEmpty()) {
            currentDeck += 1;
            if (currentDeck == model)
                checkForLeftovers();
        }
    }

    private void checkForLeftovers() {
        if (currentDeck >= examDecks.length - 1) {
            for (int i = 0; i < examDecks.length; i++) {
                if (examDecks[i].getCards().size() > 0)
                    currentDeck = i;
            }
        }
    }

    public ExamDeck[] getExamDecks() {
        return examDecks;
    }
}
