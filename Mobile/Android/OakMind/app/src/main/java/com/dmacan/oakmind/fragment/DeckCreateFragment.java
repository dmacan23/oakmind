package com.dmacan.oakmind.fragment;

import android.graphics.Color;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.dmacan.oakmind.DeckCreateActivity;
import com.dmacan.oakmind.R;
import com.dmacan.oakmind.adapter.CategorySpinnerAdapter;
import com.dmacan.oakmind.api.listener.OnDataCreatedListener;
import com.dmacan.oakmind.api.listener.OnDataMultipleReadListener;
import com.dmacan.oakmind.api.listener.OnDataUpdateListener;
import com.dmacan.oakmind.api.response.CreateResponse;
import com.dmacan.oakmind.api.response.UpdateResponse;
import com.dmacan.oakmind.content.Session;
import com.dmacan.oakmind.data.DataModel;
import com.dmacan.oakmind.data.Deck;
import com.dmacan.oakmind.data.DeckCategory;
import com.dmacan.oakmind.data.controller.DeckController;
import com.dmacan.oakmind.widget.MagicColorPickerNew;

import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by David on 23.8.2014..
 */
public class DeckCreateFragment extends OakMindFragment implements OnDataMultipleReadListener, OnDataCreatedListener, MagicColorPickerNew.OnColorSelectedListener, AdapterView.OnItemSelectedListener, OnDataUpdateListener {

    @InjectView(R.id.etDeckLabel)
    EditText etDeckLabel;
    @InjectView(R.id.etDeckDescription)
    EditText etDescription;
    @InjectView(R.id.spCategories)
    Spinner spCategories;
    @InjectView(R.id.btnCreate)
    Button btnCreate;
    @InjectView(R.id.btnColor)
    Button btnColor;

    private CategorySpinnerAdapter categorySpinnerAdapter;
    private DeckController deckController;
    private int color;
    private MagicColorPickerNew magicColorPicker;
    private Deck editDeck;

    @Override
    public int provideLayoutRes() {
        return R.layout.fragment_deck_create;
    }

    @Override
    public void main() {
        editDeck = ((DeckCreateActivity) getOakMindActivity()).getDeck();
        if (editDeck != null) {
            etDescription.setText(editDeck.getDescription());
            etDeckLabel.setText(editDeck.getLabel());
            this.color = Color.parseColor(editDeck.getColor());
            btnColor.setBackgroundColor(color);
            btnCreate.setText("Update");
        } else
            this.color = getResources().getColor(R.color.mc_indigo);
        magicColorPicker = new MagicColorPickerNew(getOakMindActivity());
        magicColorPicker.setOnColorSelectedListener(this);
        categorySpinnerAdapter = new CategorySpinnerAdapter(getOakMindActivity());
        spCategories.setEnabled(false);
        btnCreate.setEnabled(false);
        deckController = new DeckController();
        deckController.setOnDataMultipleReadListener(this);
        deckController.readCategories(false);
    }

    @OnClick(R.id.btnCreate)
    void create() {
        boolean update = true;
        if (this.editDeck == null) {
            editDeck = new Deck();
            update = false;
        }
        editDeck.setCategory((DeckCategory) spCategories.getSelectedItem());
        editDeck.setColor(String.format("#%06X", 0xFFFFFF & color));
        editDeck.setDescription(etDescription.getText().toString());
        editDeck.setLabel(etDeckLabel.getText().toString());
        if (!update) {
            editDeck.setUser(Session.readCurrentUserNew(getOakMindActivity()));
            deckController.setOnDataCreatedListener(this);
            deckController.createDeck(editDeck);
        } else {
            deckController.setOnDataUpdateListener(this);
            deckController.updateDeck(editDeck);
        }
    }


    @OnClick(R.id.btnColor)
    void pickColor() {
        displayDialog();
    }

    public void displayDialog() {
        magicColorPicker.show();
    }

    @Override
    public void onDataMultipleRead(DataModel[] result) {
        for (DataModel model : result) {
            this.categorySpinnerAdapter.addItem((DeckCategory) model);
        }
        spCategories.setEnabled(true);
        spCategories.setAdapter(categorySpinnerAdapter);
        btnCreate.setEnabled(true);
    }

    @Override
    public void onDataCreated(CreateResponse response) {
        messenger().dismissAll();
        getOakMindActivity().finish();
    }

    @Override
    public void beforeExecution() {
        messenger().showLoading("Saving deck");
    }

    @Override
    public void onColorSelected(int color) {
        this.color = color;
        this.btnColor.setBackgroundColor(color);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        DeckCategory category = (DeckCategory) spCategories.getItemAtPosition(position);
        Toast.makeText(getOakMindActivity(), category.getLabel(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onDataUpdate(UpdateResponse response) {
        messenger().dismissAll();
        getOakMindActivity().finish();
    }
}
