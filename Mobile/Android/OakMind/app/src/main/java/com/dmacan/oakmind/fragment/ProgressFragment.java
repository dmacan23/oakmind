package com.dmacan.oakmind.fragment;

import com.dmacan.oakmind.R;
import com.dmacan.oakmind.api.listener.OnDataMultipleReadListener;
import com.dmacan.oakmind.content.Session;
import com.dmacan.oakmind.data.DataModel;
import com.dmacan.oakmind.data.Exam;
import com.dmacan.oakmind.data.controller.ExamController;
import com.github.mikephil.charting.charts.BarLineChartBase;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import java.util.ArrayList;

import butterknife.InjectView;

/**
 * Created by David on 17.8.2014..
 */
public class ProgressFragment extends OakMindFragment implements OnDataMultipleReadListener {

    @InjectView(R.id.lineChartProgress)
    LineChart chart;

    private Exam[] exams;
    private ExamController examController;

    @Override
    public int provideLayoutRes() {
        return R.layout.fragment_progress;
    }

    @Override
    public void main() {
        setTitle(R.string.di_item_profile_progress);
        setupChart();
        examController = new ExamController();
        examController.setOnDataMultipleReadListener(this);
        examController.readExamsForUser(Session.readCurrentUserNew(getOakMindActivity()));
    }

    @Override
    public void onDataMultipleRead(DataModel[] result) {
        messenger().showSuccess("Your progress loaded");
        this.exams = (Exam[]) result;
        chart.setData(setupLineData(exams));
        chart.invalidate();
        chart.notifyDataSetChanged();
        chart.animateY(2000);
    }

    private void setupChart() {
        chart.setStartAtZero(true);
        chart.setDrawBorder(true);
        chart.setDescription("Exam scores through time");
        chart.setBorderPositions(new BarLineChartBase.BorderPosition[]{
                BarLineChartBase.BorderPosition.BOTTOM
        });
        chart.setDragScaleEnabled(true);
        chart.setPinchZoom(true);
        chart.setDrawGridBackground(false);
        chart.setDrawVerticalGrid(false);
        chart.setDrawYValues(false);
        chart.setDrawXLabels(false);
    }

    private LineData setupLineData(Exam[] exams) {
        ArrayList<String> xValues = new ArrayList<String>();
        ArrayList<Entry> yValues = new ArrayList<Entry>();
        String label = "Points";
        for (int i = 0; i < exams.length; i++) {
            xValues.add(String.valueOf(exams[i].getRemoteId()));
            yValues.add(new Entry((float) exams[i].getScore(), i));
        }
        LineDataSet dataSet = new LineDataSet(yValues, label);
        dataSet.setDrawFilled(true);
        dataSet.setDrawCircles(false);
        dataSet.setDrawCubic(true);
        dataSet.setColor(getResources().getColor(R.color.mc_green));
        return new LineData(xValues, dataSet);
    }

    @Override
    public void beforeExecution() {
        messenger().showLoading("Loading your progress");
    }
}
