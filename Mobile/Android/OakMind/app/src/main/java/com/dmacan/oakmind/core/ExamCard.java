package com.dmacan.oakmind.core;

import com.dmacan.oakmind.data.Card;

/**
 * Created by David on 7.9.2014..
 */
public class ExamCard {

    private String question;
    private String answer;
    private long id;
    private Card card;

    public ExamCard(long id, String question, String answer) {
        this.id = id;
        this.question = question;
        this.answer = answer;
    }

    public ExamCard(Card card) {
        this.card = card;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }
}
