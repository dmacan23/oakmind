package com.dmacan.oakmind.api.response;

import com.google.gson.annotations.Expose;

/**
 * Created by David on 14.9.2014..
 */
public class DeleteResponse extends Response {

    @Expose
    private long id;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

}
