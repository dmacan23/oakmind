package com.dmacan.oakmind.data;

import com.google.gson.annotations.Expose;

/**
 * Created by David on 8.9.2014..
 */
public class User extends DataModel {

    @Expose
    private String username;
    @Expose
    private String email;
    @Expose
    private String password;
    @Expose
    private Media avatar;

    public User() {
        super();
    }

    public User(String username, String email, String password, Media avatar) {
        super();
        this.username = username;
        this.email = email;
        this.password = password;
        this.avatar = avatar;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Media getAvatar() {
        return avatar;
    }

    public void setAvatar(Media avatar) {
        this.avatar = avatar;
    }
}
