package com.dmacan.oakmind.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by David on 13.9.2014..
 */
public class FileUploadResponse extends CreateResponse {

    @Expose
    @SerializedName("file_path")
    private String filePath;

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
}
