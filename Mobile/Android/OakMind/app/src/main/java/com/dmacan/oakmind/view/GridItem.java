package com.dmacan.oakmind.view;

import android.view.View;

import com.dmacan.oakmind.adapter.filter.FilterItem;

/**
 * Created by David on 24.8.2014..
 */
public interface GridItem extends FilterItem {

    /**
     * After inflating the View, displays data in the provided layout
     *
     * @param view          View that is inflated
     * @param position      Position of the item to be displayed
     * @param dynamicHeight Calculated dynamic height of the View
     */
    public void display(View view, int position, double dynamicHeight);

    /**
     * Provides layout resource Id for the item display
     *
     * @return layout resource Id
     */
    public int provideItemLayoutRes();

}
