package com.dmacan.oakmind.data.controller;

import retrofit.mime.TypedFile;

/**
 * Created by David on 13.9.2014..
 */
public class MediaController extends DataController {

    public MediaController() {
        super();
    }

    public void uploadImage(TypedFile file, boolean initBeforeExecution) {
        if (initBeforeExecution && getOnFileUploadListener() != null)
            getOnFileUploadListener().beforeExecution();
        getOakMindAPI().uploadFile(file, getFileUploadCallback());
    }

    public void uploadImage(TypedFile file) {
        uploadImage(file, true);
    }

}
