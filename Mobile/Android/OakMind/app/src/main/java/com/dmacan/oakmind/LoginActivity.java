package com.dmacan.oakmind;

import android.os.Bundle;

import com.dmacan.oakmind.fragment.LoginFragment;


public class LoginActivity extends OakMindActivity {

    @Override
    public int provideLayoutRes() {
        return R.layout.activity_container;
    }

    @Override
    public void main(Bundle savedInstanceState) {
        setupFragment(R.id.container, new LoginFragment());
    }

}
