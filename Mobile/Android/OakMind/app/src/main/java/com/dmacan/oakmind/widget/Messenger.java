package com.dmacan.oakmind.widget;

import android.app.Activity;
import android.graphics.Color;
import android.util.Log;

import com.dmacan.oakmind.R;
import com.github.johnpersano.supertoasts.SuperCardToast;
import com.github.johnpersano.supertoasts.SuperToast;

/**
 * Created by David on 16.8.2014..
 */
public class Messenger extends SuperCardToast {

    private Activity activity;
    private SuperToast.Type type;

    public Messenger(Activity activity, SuperToast.Type type) {
        super(activity, type);
        this.activity = activity;
        this.type = type;
        init();
    }

    public void show(Object message) {
        Log.d("Msg", message.toString());
        this.setText(String.valueOf(message));
        if (!this.isShowing())
            this.show();
    }


    private void init() {
        this.setDuration(Defaults.DURATION);
        this.setBackground(Defaults.BACKGROUND);
        this.setTextColor(Defaults.TEXT_COLOR);
        this.setSwipeToDismiss(Defaults.SWIPE_TO_DISMISS);
        this.setAnimations(Defaults.ANIMATION);
        // this.setTextSize((int) setupTextSize());
    }

    private static class Defaults {
        public static final int DURATION = SuperToast.Duration.SHORT;
        public static final int BACKGROUND = SuperToast.Background.BLUE;
        public static final int TEXT_COLOR = Color.WHITE;
        public static final boolean SWIPE_TO_DISMISS = false;
        public static final SuperToast.Animations ANIMATION = SuperToast.Animations.POPUP;
    }


    private float setupTextSize() {
        return activity.getResources().getDimension(R.dimen.toast_text);
    }


}
