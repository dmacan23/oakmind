package com.dmacan.oakmind.view;

/**
 * Created by David on 27.8.2014..
 */
public interface PagerFragment {

    public String provideLabel();

    public void setLabel(String label);

}
