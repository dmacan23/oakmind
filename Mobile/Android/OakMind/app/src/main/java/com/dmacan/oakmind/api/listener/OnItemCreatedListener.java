package com.dmacan.oakmind.api.listener;

import com.dmacan.oakmind.api.response.CreateResponse;

/**
 * Created by David on 7.9.2014..
 */
public interface OnItemCreatedListener {
    public void onItemCreated(CreateResponse response);
}
