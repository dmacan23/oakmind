package com.dmacan.oakmind.data;

import com.google.gson.annotations.Expose;

/**
 * Created by David on 8.9.2014..
 */
public class Exam extends DataModel {

    @Expose
    private User user;
    @Expose
    private Deck deck;
    @Expose
    private double score;
    @Expose
    private ExamType type;

    public Exam() {
        super();
    }

    public Exam(User user, Deck deck, double score, ExamType type) {
        super();
        this.user = user;
        this.deck = deck;
        this.score = score;
        this.type = type;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Deck getDeck() {
        return deck;
    }

    public void setDeck(Deck deck) {
        this.deck = deck;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    public ExamType getType() {
        return type;
    }

    public void setType(ExamType type) {
        this.type = type;
    }
}
