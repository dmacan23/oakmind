package com.dmacan.oakmind.widget;

import android.view.View;

import com.dmacan.oakmind.R;
import com.larswerkman.holocolorpicker.ColorPicker;
import com.larswerkman.holocolorpicker.OpacityBar;
import com.larswerkman.holocolorpicker.SVBar;
import com.larswerkman.holocolorpicker.SaturationBar;

/**
 * Created by David on 23.8.2014..
 */
public class MagicColorPicker {

    private View v;
    private ColorPicker picker;
    private SVBar svBar;
    private OpacityBar opacityBar;
    private SaturationBar saturationBar;

    public MagicColorPicker(View v) {
        this.v = v;
        init();
    }

    public MagicColorPicker(MagicDialog dialog) {
        this.picker = (ColorPicker) dialog.findViewById(R.id.picker);
        this.svBar = (SVBar) dialog.findViewById(R.id.svbar);
        this.opacityBar = (OpacityBar) dialog.findViewById(R.id.opacitybar);
        this.saturationBar = (SaturationBar) dialog.findViewById(R.id.saturationbar);
    }

    private void init() {
        picker = (ColorPicker) v.findViewById(R.id.picker);
        svBar = (SVBar) v.findViewById(R.id.svbar);
        opacityBar = (OpacityBar) v.findViewById(R.id.opacitybar);
        saturationBar = (SaturationBar) v.findViewById(R.id.saturationbar);
        picker.setOldCenterColor(picker.getColor());
    }

    public ColorPicker getPicker() {
        return picker;
    }

    public void setPicker(ColorPicker picker) {
        this.picker = picker;
    }

    public SVBar getSvBar() {
        return svBar;
    }

    public void setSvBar(SVBar svBar) {
        this.svBar = svBar;
    }

    public OpacityBar getOpacityBar() {
        return opacityBar;
    }

    public void setOpacityBar(OpacityBar opacityBar) {
        this.opacityBar = opacityBar;
    }

    public SaturationBar getSaturationBar() {
        return saturationBar;
    }

    public void setSaturationBar(SaturationBar saturationBar) {
        this.saturationBar = saturationBar;
    }
}
