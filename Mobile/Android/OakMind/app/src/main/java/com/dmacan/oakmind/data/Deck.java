package com.dmacan.oakmind.data;

import com.google.gson.annotations.Expose;

import java.util.List;

/**
 * Created by David on 8.9.2014..
 */
public class Deck extends DataModel {

    @Expose
    private String label;
    @Expose
    private String description;
    @Expose
    private String color;
    @Expose
    private User user;
    @Expose
    private DeckCategory category;
    @Expose
    private List<Card> cards;

    public Deck() {
        super();
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public DeckCategory getCategory() {
        return category;
    }

    public void setCategory(DeckCategory category) {
        this.category = category;
    }

    public List<Card> getCards() {
        return cards;
    }

    public void setCards(List<Card> cards) {
        this.cards = cards;
    }
}
