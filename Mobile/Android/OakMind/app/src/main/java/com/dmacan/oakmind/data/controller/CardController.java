package com.dmacan.oakmind.data.controller;

import com.dmacan.oakmind.api.request.CreateRequestNew;
import com.dmacan.oakmind.api.request.ReadRequestNew;
import com.dmacan.oakmind.api.request.UpdateRequest;
import com.dmacan.oakmind.data.Card;
import com.dmacan.oakmind.data.Deck;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by David on 8.9.2014..
 */
public class CardController extends DataController {

    private Callback<Card[]> readCardsCallback = new Callback<Card[]>() {
        @Override
        public void success(Card[] cards, Response response) {
            if (getOnDataMultipleReadListener() != null)
                getOnDataMultipleReadListener().onDataMultipleRead(cards);
        }

        @Override
        public void failure(RetrofitError error) {
            Crouton.cancelAllCroutons();
            if (getOnNetworkErrorListener() != null)
                getOnNetworkErrorListener().onNetworkError(error);
        }
    };

    public CardController() {
        super();
    }

    public void readCards(Deck deck, boolean initBeforeExecution) {
        if (initBeforeExecution && getOnDataMultipleReadListener() != null)
            getOnDataMultipleReadListener().beforeExecution();
        ReadRequestNew requestNew = new ReadRequestNew();
        requestNew.setDataModel(deck);
        requestNew.setMethod("cardsForDeck");
        getOakMindAPI().readCards(requestNew, readCardsCallback);
    }

    public void readCards(Deck deck) {
        readCards(deck, true);
    }

    public void createCard(Card card, boolean initBeforeExecution) {
        if (initBeforeExecution && getOnDataCreatedListener() != null)
            getOnDataCreatedListener().beforeExecution();
        CreateRequestNew requestNew = new CreateRequestNew();
        requestNew.setDataModel(card);
        requestNew.setMethod("card");
        getOakMindAPI().createItem(requestNew, getCreateCallback());
    }

    public void createCard(Card card) {
        createCard(card, true);
    }

    public void deleteCard(Card card, boolean initBeforeExecution) {
        if (initBeforeExecution && getOnDataDeletedListener() != null)
            getOnDataDeletedListener().beforeExecution();
        UpdateRequest request = new UpdateRequest();
        request.setMethod("deleteCard");
        request.setDataModel(card);
        getOakMindAPI().updateData(request, getUpdateCallback());
    }

    public void deleteCard(Card card) {
        deleteCard(card);
    }
}
