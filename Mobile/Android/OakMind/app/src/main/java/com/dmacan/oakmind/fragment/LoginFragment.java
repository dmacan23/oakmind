package com.dmacan.oakmind.fragment;

import android.content.Intent;
import android.widget.EditText;
import android.widget.TextView;

import com.dmacan.oakmind.MainActivity;
import com.dmacan.oakmind.R;
import com.dmacan.oakmind.RegisterActivity;
import com.dmacan.oakmind.api.listener.OnDataReadListener;
import com.dmacan.oakmind.content.Session;
import com.dmacan.oakmind.data.DataModel;
import com.dmacan.oakmind.data.User;
import com.dmacan.oakmind.data.controller.UserController;
import com.joanzapata.android.iconify.Iconify;

import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by David on 9.8.2014..
 */
public class LoginFragment extends OakMindFragment implements OnDataReadListener {

    @InjectView(R.id.etLoginUsername)
    EditText etUsername;
    @InjectView(R.id.etLoginPassword)
    EditText etPassword;
    @InjectView(R.id.txtIconLoginUsername)
    TextView iconUsername;
    @InjectView(R.id.txtIconLoginPassword)
    TextView iconPassword;

    private UserController userController;

    @Override
    public int provideLayoutRes() {
        return R.layout.fragment_login;
    }

    @Override
    public void main() {
        setTitle(R.string.label_login);
        Iconify.addIcons(iconUsername, iconPassword, etUsername, etPassword);
        userController = new UserController();
        userController.setOnDataReadListener(this);
    }

    @OnClick(R.id.btnLogin)
    void login() {
        userController.readUser(getOakMindActivity(), etUsername.getText().toString(), etPassword.getText().toString());
    }

    @OnClick(R.id.txtLoginSignup)
    void signUp() {
        startActivity(new Intent(getOakMindActivity(), RegisterActivity.class));
    }

    @Override
    public void onDataRead(DataModel result) {
        messenger().dismissAll();
        User user = (User) result;
        if (user.getRemoteId() > 0) {
            Session.saveCurrentUserNew(getOakMindActivity(), (User) result);
            startActivity(new Intent(getOakMindActivity(), MainActivity.class));
            getOakMindActivity().finish();
        } else
            messenger().showError("Login failed");
    }

    @Override
    public void beforeExecution() {
        messenger().showLoading("Logging you in");
    }
}
