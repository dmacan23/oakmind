package com.dmacan.oakmind.fragment;

import android.view.View;
import android.widget.AdapterView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.dmacan.oakmind.DeckActivity;
import com.dmacan.oakmind.R;
import com.dmacan.oakmind.adapter.GridAdapter;
import com.dmacan.oakmind.api.listener.OnDataDeletedListener;
import com.dmacan.oakmind.api.listener.OnDataMultipleReadListener;
import com.dmacan.oakmind.api.response.DeleteResponse;
import com.dmacan.oakmind.data.Card;
import com.dmacan.oakmind.data.DataModel;
import com.dmacan.oakmind.data.Deck;
import com.dmacan.oakmind.data.controller.CardController;
import com.dmacan.oakmind.util.Const;
import com.dmacan.oakmind.view.presenter.CardPresenter;
import com.dmacan.oakmind.widget.ConfirmDialog;
import com.etsy.android.grid.StaggeredGridView;
import com.melnykov.fab.FloatingActionButton;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.InjectView;
import butterknife.OnClick;
import hr.mtlab.dmacan.magicui.graphics.MagicDrawable;

/**
 * Created by David on 24.8.2014..
 */
public class DeckFragment extends OakMindFragment implements AdapterView.OnItemClickListener, OnDataMultipleReadListener, ConfirmDialog.OnDialogConfirmListener, OnDataDeletedListener {

    @InjectView(R.id.btnFAB)
    FloatingActionButton fab;
    @InjectView(R.id.grid)
    StaggeredGridView gridView;
    @InjectView(R.id.emptyContainerPlaceholder)
    View emptyPlaceholder;

    private GridAdapter adapter;
    private boolean isOwned;
    private Deck deck;
    private CardController cardController;
    private ConfirmDialog confirmDialog;
    private Card selectedCard;
    private AdapterView.OnItemLongClickListener onItemLongClickListener = new AdapterView.OnItemLongClickListener() {
        @Override
        public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
           /* Toast.makeText(getOakMindActivity(), "Long klik", Toast.LENGTH_SHORT).show();
            selectedCard = deck.getCards().get(position);
            confirmDialog.setDialogTitle("Delete card");
            confirmDialog.setDialogMessage("Are you sure you want to do this?");
            confirmDialog.show();
            return false;*/
            return true;
        }
    };

    @Override
    public int provideLayoutRes() {
        return R.layout.grid_layout_3_col;
    }

    @Override
    public void main() {
        confirmDialog = new ConfirmDialog(getOakMindActivity());
        confirmDialog.setOnDialogConfirmListener(this);
        deck = ((DeckActivity) getOakMindActivity()).getDeck();
        isOwned = getOakMindActivity().getIntent().getExtras().getBoolean(Const.Key.DECK_OWNED, false);
        adapter = new GridAdapter(getOakMindActivity());
//        adapter.enableSearch(getOakMindActivity(), getMenu(), false);
        adapter.setHeightModifier(1.5);
        gridView.setAdapter(adapter);
        gridView.setLongClickable(true);
        gridView.setOnItemLongClickListener(onItemLongClickListener);
        gridView.setOnItemClickListener(this);
        gridView.setEmptyView(emptyPlaceholder);
        if (isOwned) {
            fab.setImageDrawable(new MagicDrawable(getOakMindActivity(), getResources().getString(R.string.fa_pen)).actionBarSize().colorRes(R.color.white));
            fab.attachToListView(gridView);
        } else
            fab.setVisibility(View.GONE);
        if (deck.getCards() == null || deck.getCards().size() == 0)
            loadData();
        else
            fillAdapter(deck.getCards());
    }

    @OnClick(R.id.btnFAB)
    void createCard() {
        ((DeckActivity) getOakMindActivity()).startCardCreateActivity();
    }

    private void loadData() {
        cardController = new CardController();
        cardController.setOnDataMultipleReadListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        CardPresenter presenter = (CardPresenter) adapter.getItem(position);
        presenter.toggleQA();
        presenter.display(view, position, 0);
        YoYo.with(Techniques.FlipInX).duration(300).playOn(view);
    }

    @Override
    public void onDataMultipleRead(DataModel[] result) {
        messenger().dismissAll();
        messenger().showSuccess("Cards loaded");
        adapter.empty();
        if (result != null && result.length > 0) {
            List<Card> cards = new ArrayList<Card>(Arrays.asList((Card[]) result));
            deck.setCards(cards);
            fillAdapter(cards);
        }
    }

    private void fillAdapter(List<Card> cards) {
        for (Card card : cards) {
            adapter.addItem(new CardPresenter(card));
        }
    }

    @Override
    public void beforeExecution() {
        messenger().showLoading("Loading cards");
    }

    @Override
    public void onResume() {
        super.onResume();
        if (cardController != null)
            cardController.readCards(deck);
    }

    @Override
    public void onDialogConfirm(boolean positive) {
        if (positive)
            cardController.deleteCard(selectedCard);
        confirmDialog.dismiss();
    }

    @Override
    public void onDataDeleted(DeleteResponse response) {
        cardController.readCards(deck);
    }
}
