package com.dmacan.oakmind.data;

import com.google.gson.annotations.Expose;

/**
 * Created by David on 8.9.2014..
 */
public class MediaType extends DataModel {

    @Expose
    private String label;

    public MediaType() {
        super();
    }

    public MediaType(long id) {
        setRemoteId(id);
    }

    public MediaType(String label) {
        super();
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
