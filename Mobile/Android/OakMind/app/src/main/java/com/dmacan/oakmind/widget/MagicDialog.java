package com.dmacan.oakmind.widget;

import android.content.Context;
import android.view.View;

import com.gitonway.lee.niftymodaldialogeffects.lib.NiftyDialogBuilder;

/**
 * Created by David on 23.8.2014..
 */
public class MagicDialog {

    private Context context;
    private int layout;
    private NiftyDialogBuilder dialogBuilder;

    public MagicDialog(Context context, int layout) {
        this.context = context;
        this.layout = layout;
        init();
        dialogBuilder.setCustomView(layout, context);
    }

    public MagicDialog(Context context) {
        this.context = context;
        init();
    }

    private void init() {
        this.dialogBuilder = NiftyDialogBuilder.getInstance(context);
        dialogBuilder.withMessage(null);
        dialogBuilder.withTitle(null);
    }

    public void show() {
        dialogBuilder.show();
    }

    public void dismiss() {
        dialogBuilder.dismiss();
    }

    public View findViewById(int id) {
        return dialogBuilder.findViewById(id);
    }

    public Context getContext() {
        return context;
    }

    public NiftyDialogBuilder getDialogBuilder() {
        return dialogBuilder;
    }

    /*
    dialogBuilder
                .withTitle("Modal Dialog")                                  //.withTitle(null)  no title
                .withTitleColor("#FFFFFF")                                  //def
                .withDividerColor("#11000000")                              //def
                .withMessage("This is a modal Dialog.")                     //.withMessage(null)  no Msg
                .withMessageColor("#FFFFFF")                                //def
                .withIcon(getResources().getDrawable(R.drawable.icon))
                .isCancelableOnTouchOutside(true)                           //def    | isCancelable(true)
                .withDuration(700)                                          //def
                .withEffect(effect)                                         //def Effectstype.Slidetop
                .withButton1Text("OK")                                      //def gone
                .withButton2Text("Cancel")                                  //def gone
                .setCustomView(R.layout.custom_view,v.getContext())         //.setCustomView(View or ResId,context)
                .setButton1Click(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(v.getContext(), "i'm btn1", Toast.LENGTH_SHORT).show();
                    }
                })
                .setButton2Click(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(v.getContext(), "i'm btn2", Toast.LENGTH_SHORT).show();
                    }
                })
                .show();
     */


}
