package com.dmacan.oakmind.adapter.filter;

/**
 * Created by David on 24.8.2014..
 */
public interface FilterItem {

    public String filterableString();

}
