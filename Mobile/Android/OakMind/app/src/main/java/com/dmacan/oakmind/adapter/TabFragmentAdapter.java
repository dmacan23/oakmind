package com.dmacan.oakmind.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.dmacan.oakmind.view.PagerFragment;

import java.util.List;

/**
 * Created by David on 27.8.2014..
 */
public class TabFragmentAdapter extends OakMindPagerAdapter {

    public TabFragmentAdapter(FragmentManager fm, List<Fragment> items) {
        super(fm, items);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return ((PagerFragment) this.getItem(position)).provideLabel();
    }
}
