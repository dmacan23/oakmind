package com.dmacan.oakmind.api.request;

import com.dmacan.oakmind.data.DataModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by David on 8.9.2014..
 */
public class ReadRequestNew extends Request {

    @Expose
    @SerializedName("params")
    private DataModel dataModel;

    public ReadRequestNew() {
        super();
    }

    public ReadRequestNew(String method) {
        super(method);
    }

    public DataModel getDataModel() {
        return dataModel;
    }

    public void setDataModel(DataModel dataModel) {
        this.dataModel = dataModel;
    }
}
