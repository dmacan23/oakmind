package com.dmacan.oakmind.fragment;

import com.dmacan.oakmind.R;

/**
 * Created by David on 17.8.2014..
 */
public class SettingsFragment extends OakMindFragment {
    @Override
    public int provideLayoutRes() {
        return R.layout.fragment_settings;
    }

    @Override
    public void main() {
        setTitle(R.string.di_item_oakmind_settings);
    }
}
