package com.dmacan.oakmind.view.presenter;

import android.graphics.Color;
import android.text.Html;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dmacan.oakmind.R;
import com.dmacan.oakmind.data.Deck;
import com.dmacan.oakmind.view.GridItem;
import com.etsy.android.grid.util.DynamicHeightTextView;

import hr.mtlab.dmacan.magicui.widget.util.MagicFont;

/**
 * Created by David on 7.9.2014..
 */
public class DeckPresenter implements GridItem {

    private Deck deck;

    public DeckPresenter(Deck deck) {
        this.deck = deck;
    }

    public Deck getDeck() {
        return deck;
    }

    public void setDeck(Deck deck) {
        this.deck = deck;
    }

    @Override
    public void display(View view, int position, double dynamicHeight) {
        TextView txtLabel = (TextView) view.findViewById(R.id.giTitle);
        DynamicHeightTextView txtIcon = (DynamicHeightTextView) view.findViewById(R.id.giSymbol);
        LinearLayout background = (LinearLayout) view.findViewById(R.id.giLayout);
        txtLabel.setText(deck.getLabel());
        background.setBackgroundColor(Color.parseColor(deck.getColor()));
        if (deck.getCategory() != null)
            txtIcon.setText(Html.fromHtml(deck.getCategory().getSymbol()));
        txtIcon.setHeightRatio(dynamicHeight);
        MagicFont.setFont("fontawesome.ttf", txtIcon);
    }

    @Override
    public int provideItemLayoutRes() {
        return R.layout.grid_item_simple;
    }

    @Override
    public String filterableString() {
        return deck.getLabel();
    }
}
