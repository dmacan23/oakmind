package com.dmacan.oakmind.widget;

import android.content.Context;
import android.view.View;
import android.widget.AdapterView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.dmacan.oakmind.R;
import com.nineoldandroids.animation.Animator;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by David on 14.9.2014..
 */
public class MagicColorPickerNew extends MagicDialog implements AdapterView.OnItemClickListener, Animator.AnimatorListener {

    private hr.mtlab.dmacan.magicui.widget.colorpicker.MagicColorPicker colorPicker;
    private OnColorSelectedListener onColorSelectedListener;
    private List<Integer> colors;

    public MagicColorPickerNew(Context context) {
        super(context, R.layout.dialog_magic_color_picker);
        init();
    }

    private void init() {
        colorPicker = (hr.mtlab.dmacan.magicui.widget.colorpicker.MagicColorPicker) findViewById(R.id.colorPicker);
        colors = generateColors();
        colorPicker.setColors(colors);
        colorPicker.setOnItemClickListener(this);
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        YoYo.AnimationComposer animation = YoYo.with(Techniques.RubberBand).duration(300);
        animation.withListener(this);
        animation.playOn(view);
        if (this.onColorSelectedListener != null)
            this.onColorSelectedListener.onColorSelected(colors.get(position));
    }

    public void setOnColorSelectedListener(OnColorSelectedListener onColorSelectedListener) {
        this.onColorSelectedListener = onColorSelectedListener;
    }

    @Override
    public void onAnimationStart(Animator animator) {

    }

    @Override
    public void onAnimationEnd(Animator animator) {
        this.dismiss();
    }

    @Override
    public void onAnimationCancel(Animator animator) {

    }

    @Override
    public void onAnimationRepeat(Animator animator) {

    }

    private List<Integer> generateColors() {
        int[] colorArray = getContext().getResources().getIntArray(R.array.material_colors);
        colors = new ArrayList<Integer>();
        for (int color : colorArray)
            colors.add(Integer.valueOf(color));
        return colors;
    }

    public interface OnColorSelectedListener {
        public void onColorSelected(int color);
    }
}
