package com.dmacan.oakmind.util;

/**
 * Created by David on 10.8.2014..
 */
public class Const {

    public static class Settings {
        public static final String DEBUG_TAG = "DAM";
        public static final String RETROFIT_DEBUG = "RETRO";
        public static final boolean DEBUG_MODE = true;
        public static final String LOGIN_USERNAME = "dmacan";
        public static final String LOGIN_PASSWORD = "xxx123";
        public static final int MODEL_SIMPLE = 2;
        public static final int MODEL_ADVANCED = 5;
    }

    public static class API {
        public static final String LOCATION = "http://ars-pingo.com/oakmind/src/api";
        public static final String REGISTER = "/register.php";
        public static final String LOGIN = "/login.php";
        public static final String CATALOGUES = "/catalogues.php";
        public static final String CREATE_CARD = "/card_create.php";
        public static final String CREATE_DECK = "/deck_create.php";
        public static final String READ_DECKS = "/deck_read.php";
        public static final String READ_PUBLIC_DECKS = "/deck_read_public.php";
        public static final String READ_CARDS = "/card_read.php";
        public static final String DELETE_DECK = "/deck_delete.php";
        public static final String DATA_READ = "/data_read.php";

        public static class Request {
            public static final String CATALOGUES = "catalogues_request";
        }
    }

    public static class Duration {
        public static final int ANIMATION_FLIP = 300;
        public static final int ANIMATION_SIDE_ZOOM = 500;
        public static final int SPLASH = 1500;
    }

    public static class Request {
        public static final int FILE = 123;
    }

    public static class Values {
        public static final String EMPTY = "";
    }

    public static class Regex {
        public static final String ALPHANUMERIC = "^[a-zA-Z0-9_]*$";
        public static final String EMAIL = "^[_A-Za-z0-9-\\\\+]+(\\\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\\\.[A-Za-z0-9]+)*(\\\\.[A-Za-z]{2,})$;";
    }

    public static class Status {
        public static final String REGISTRATION_OK = "OK";
    }

    public static class Format {
        public static final String DATETIME = "YYYY-MM-DD HH:MM:SS";
    }

    public static class Key {
        public static final String EDITABLE = "iAmEditable";
        public static final String DECK_OWNED = "deckOwned";
        public static final String DECK_CURRENT = "currentDeck";
        public static final String DECK_SYMBOL = "deckSymbol";
        public static final String USER_CURRENT = "currentUser";
        public static final String DECK_CURRENT_NEW = "currentDeck";
    }
}
