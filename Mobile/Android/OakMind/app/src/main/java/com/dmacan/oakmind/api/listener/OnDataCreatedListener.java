package com.dmacan.oakmind.api.listener;

import com.dmacan.oakmind.api.response.CreateResponse;

/**
 * Created by David on 13.9.2014..
 */
public interface OnDataCreatedListener extends OnDataCRUDListener {

    public void onDataCreated(CreateResponse response);

}
