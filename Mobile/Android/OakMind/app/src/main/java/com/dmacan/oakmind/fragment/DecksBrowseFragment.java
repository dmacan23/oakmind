package com.dmacan.oakmind.fragment;


import android.view.View;

import com.dmacan.oakmind.R;
import com.dmacan.oakmind.api.listener.OnDataMultipleReadListener;
import com.dmacan.oakmind.data.DataModel;
import com.dmacan.oakmind.data.Deck;
import com.dmacan.oakmind.data.controller.DeckController;
import com.dmacan.oakmind.view.presenter.DeckPresenter;

/**
 * Created by David on 17.8.2014..
 */
public class DecksBrowseFragment extends DeckGridFragment implements OnDataMultipleReadListener {

    private DeckController deckController;

    @Override
    public void loadData() {
        setTitle(R.string.di_item_deck_browse);
        fab.setVisibility(View.GONE);
        deckController = new DeckController();
        deckController.setOnDataMultipleReadListener(this);
        deckController.readDecksPublic();
    }

    @Override
    public void onDataMultipleRead(DataModel[] result) {
        messenger().dismissAll();
        for (DataModel d : result) {
            Deck deck = (Deck) d;
            adapter.addItem(new DeckPresenter(deck));
        }
        messenger().showSuccess("Decks loaded");
    }

    @Override
    public void beforeExecution() {
        messenger().showLoading("Loading public decks");
    }


}

