package com.dmacan.oakmind.fragment;

import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.dmacan.oakmind.DeckActivity;
import com.dmacan.oakmind.R;
import com.dmacan.oakmind.api.listener.OnDataCreatedListener;
import com.dmacan.oakmind.api.response.CreateResponse;
import com.dmacan.oakmind.content.Session;
import com.dmacan.oakmind.core.ExamCalculator;
import com.dmacan.oakmind.core.ExamCard;
import com.dmacan.oakmind.core.Leitner;
import com.dmacan.oakmind.data.Card;
import com.dmacan.oakmind.data.Exam;
import com.dmacan.oakmind.data.ExamType;
import com.dmacan.oakmind.data.controller.ExamController;
import com.dmacan.oakmind.util.Const;
import com.nineoldandroids.animation.Animator;

import java.util.ArrayList;
import java.util.List;

import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by David on 3.9.2014..
 */
public class ExamFragment extends OakMindFragment implements OnDataCreatedListener, Animator.AnimatorListener {

    @InjectView(R.id.cardLayout)
    RelativeLayout cardLayout;
    @InjectView(R.id.cardSideTitle)
    TextView txtCardSideTitle;
    @InjectView(R.id.cardSideIndicator)
    TextView txtCardSideIndicator;
    @InjectView(R.id.btnCorrect)
    Button btnCorrect;
    @InjectView(R.id.btnIncorrect)
    Button btnIncorrect;

    private ExamCard currentCard;
    private Leitner leitner;
    private ExamCalculator calculator;
    private ExamController examController;
    private boolean examCompleted = false;

    @Override
    public int provideLayoutRes() {
        return R.layout.fragment_exam;
    }

    @Override
    public void main() {
        setTitle(R.string.di_exam);
        examController = new ExamController();
        examController.setOnDataCreatedListener(this);
        leitner = new Leitner();
        calculator = new ExamCalculator();
        leitner.startExam(generateExamCards(), Leitner.MODEL_SIMPLE);
        prepareQuestion();
    }

    private List<ExamCard> generateExamCards() {
        List<Card> cards = ((DeckActivity) getOakMindActivity()).getDeck().getCards();
        List<ExamCard> examCards = new ArrayList<ExamCard>();
        for (Card c : cards)
            examCards.add(new ExamCard(c.getRemoteId(), c.getQuestion().getTitle(), c.getAnswer().getTitle()));
        return examCards;
    }

    @OnClick(R.id.btnCorrect)
    void answerCorrect() {
        answer(true);
    }

    @OnClick(R.id.btnIncorrect)
    void answerIncorrect() {
        answer(false);
    }

    @OnClick(R.id.cardLayout)
    void flipCard() {
        YoYo.with(Techniques.FlipInX).duration(Const.Duration.ANIMATION_FLIP).playOn(cardLayout);
        toggleCardSide();
    }

    private void answer(boolean correct) {
        if (!examCompleted) {
            if (!correct) {
                leitner.answerIncorrect(currentCard);
                calculator.addCorrect();
            } else {
                leitner.answerCorrect(currentCard);
                calculator.addIncorrect();
            }
            this.examCompleted = leitner.isExamCompleted();
            YoYo.with(Techniques.ZoomOutRight).duration(Const.Duration.ANIMATION_SIDE_ZOOM).withListener(this).playOn(cardLayout);
            if (!examCompleted) {
                YoYo.with(Techniques.ZoomInLeft).duration(Const.Duration.ANIMATION_SIDE_ZOOM).delay(Const.Duration.ANIMATION_SIDE_ZOOM).playOn(cardLayout);
            }
        }
    }


    private void prepareQuestion() {
        currentCard = leitner.drawCard();
        if (leitner.isExamCompleted()) {
            Exam exam = new Exam();
            exam.setDeck(((DeckActivity) getOakMindActivity()).getDeck());
            exam.setUser(Session.readCurrentUserNew(getOakMindActivity()));
            exam.setScore(calculator.calculatePoints());
            exam.setType(new ExamType(1));
            examController.createExam(exam);
            examCompleted = true;
        } else {
            txtCardSideIndicator.setText("Q");
            txtCardSideTitle.setText(currentCard.getQuestion());
        }

    }

    private void toggleCardSide() {
        if (this.txtCardSideIndicator.getText().toString().equals("Q")) {
            this.txtCardSideIndicator.setText("A");
            this.txtCardSideTitle.setText(currentCard.getAnswer());
        } else {
            this.txtCardSideIndicator.setText("Q");
            this.txtCardSideTitle.setText(currentCard.getQuestion());
        }
    }

    @Override
    public void onDataCreated(CreateResponse response) {
        messenger().showSuccess("Exam saved");
        ((DeckActivity) getOakMindActivity()).showDefaultFragment();
    }

    @Override
    public void beforeExecution() {
        messenger().showLoading("Saving exam");
        btnCorrect.setEnabled(false);
        btnIncorrect.setEnabled(false);
        cardLayout.setVisibility(View.GONE);
    }

    @Override
    public void onAnimationStart(Animator animator) {
    }

    @Override
    public void onAnimationEnd(Animator animator) {
        prepareQuestion();
    }

    @Override
    public void onAnimationCancel(Animator animator) {

    }

    @Override
    public void onAnimationRepeat(Animator animator) {

    }
}
