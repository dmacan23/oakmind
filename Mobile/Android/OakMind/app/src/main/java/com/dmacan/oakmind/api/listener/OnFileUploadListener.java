package com.dmacan.oakmind.api.listener;

import com.dmacan.oakmind.api.response.FileUploadResponse;

/**
 * Created by David on 13.9.2014..
 */
public interface OnFileUploadListener extends OnDataCRUDListener {
    public void onFileUploaded(FileUploadResponse response);
}
