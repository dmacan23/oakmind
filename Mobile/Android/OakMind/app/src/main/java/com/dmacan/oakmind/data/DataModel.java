package com.dmacan.oakmind.data;

import com.dmacan.oakmind.util.APIUtil;
import com.dmacan.oakmind.util.Const;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.joda.time.DateTime;

/**
 * Created by David on 8.9.2014..
 */
public class DataModel {

    @Expose
    @SerializedName("id")
    private long remoteId;
    @Expose
    private String createdAt;
    @Expose
    private String updatedAt;

    public DataModel() {
        this.createdAt = DateTime.now().toString(Const.Format.DATETIME);
        this.updatedAt = DateTime.now().toString(Const.Format.DATETIME);
    }

    public long getRemoteId() {
        return remoteId;
    }

    public void setRemoteId(long remoteId) {
        this.remoteId = remoteId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String toJSON() {
        return APIUtil.createGson().toJson(this);
    }

    public DataModel fromJSON(String JSON) {
        return APIUtil.createGson().fromJson(JSON, getClass());
    }
}
