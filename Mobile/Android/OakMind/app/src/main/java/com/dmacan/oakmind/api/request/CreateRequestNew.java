package com.dmacan.oakmind.api.request;

import com.dmacan.oakmind.data.DataModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by David on 13.9.2014..
 */
public class CreateRequestNew extends Request {

    @Expose
    @SerializedName("params")
    private DataModel dataModel;

    public CreateRequestNew() {
        super();
    }

    public CreateRequestNew(String method) {
        super(method);
    }

    public DataModel getDataModel() {
        return dataModel;
    }

    public void setDataModel(DataModel dataModel) {
        this.dataModel = dataModel;
    }
}
