package com.dmacan.oakmind.data.controller;

import android.content.Context;

import com.dmacan.oakmind.api.request.CreateRequestNew;
import com.dmacan.oakmind.api.request.ReadRequestNew;
import com.dmacan.oakmind.data.User;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by David on 8.9.2014..
 */
public class UserController extends DataController {

    private Callback<User> readUserCallback = new Callback<User>() {
        @Override
        public void success(User user, Response response) {
            if (getOnDataReadListener() != null)
                getOnDataReadListener().onDataRead(user);
        }

        @Override
        public void failure(RetrofitError error) {
            Crouton.cancelAllCroutons();
            if (getOnNetworkErrorListener() != null)
                getOnNetworkErrorListener().onNetworkError(error);
        }
    };

    public UserController() {
        super();
    }

    public void readUser(Context context, String username, String password, boolean initBeforeExecution) {
        if (isNetworkConnectionAvailable(context)) {
            if (initBeforeExecution && getOnDataReadListener() != null)
                getOnDataReadListener().beforeExecution();
            ReadRequestNew request = new ReadRequestNew();
            User u = new User();
            u.setUsername(username);
            u.setPassword(password);
            request.setDataModel(u);
            request.setMethod("userLogin");
            getOakMindAPI().readUser(request, readUserCallback);
        }
    }

    public void readUser(Context context, String username, String password) {
        readUser(context, username, password, true);
    }

    public void createUser(Context context, User user) {
        createUser(context, user, true);
    }

    public void createUser(Context context, User user, boolean initBeforeExecution) {
        if (isNetworkConnectionAvailable(context)) {
            if (initBeforeExecution && (getOnDataCreatedListener() != null))
                getOnDataCreatedListener().beforeExecution();
            CreateRequestNew request = new CreateRequestNew();
            request.setDataModel(user);
            request.setMethod("user");
            getOakMindAPI().createItem(request, getCreateCallback());
        }
    }

}
