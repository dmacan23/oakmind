package com.dmacan.oakmind.api.response;

import com.google.gson.annotations.Expose;

/**
 * Created by David on 13.9.2014..
 */
public class Response {

    @Expose
    private String method;
    @Expose
    private String message;
    @Expose
    private long code;

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getCode() {
        return code;
    }

    public void setCode(long code) {
        this.code = code;
    }
}
