package com.dmacan.oakmind.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

/**
 * Created by David on 27.8.2014..
 */
public abstract class OakMindPagerAdapter extends FragmentPagerAdapter {

    private List<Fragment> items;

    public OakMindPagerAdapter(FragmentManager fm, List<Fragment> items) {
        super(fm);
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Fragment getItem(int i) {
        return items.get(i);
    }
}
