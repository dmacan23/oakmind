package com.dmacan.oakmind.widget.drawer;

import android.content.Context;

/**
 * Created by David on 17.8.2014..
 */
public class DrawerData {

    public static final String DEFAULT_TAG = "default";
    public static Context context;

    private static String getString(int id) {
        return context.getResources().getString(id);
    }

    public static DrawerItemSimple newItem(int labelID, int iconID) {
        return new DrawerItemSimple(getString(labelID), getString(iconID));
    }

    public static DrawerItemSimple newItem(int labelID, int iconID, boolean fragment) {
        return new DrawerItemSimple(getString(labelID), getString(iconID), fragment);
    }

    public static DrawerItemCategory newCategory(int labelID) {
        return new DrawerItemCategory(getString(labelID));
    }

}
