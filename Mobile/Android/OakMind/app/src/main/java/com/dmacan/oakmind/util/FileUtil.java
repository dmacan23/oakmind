package com.dmacan.oakmind.util;

import android.app.Activity;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.webkit.MimeTypeMap;

import com.kbeanie.imagechooser.api.ChooserType;
import com.kbeanie.imagechooser.api.ImageChooserListener;
import com.kbeanie.imagechooser.api.ImageChooserManager;
import com.nononsenseapps.filepicker.FilePickerActivity;

import java.io.File;
import java.util.ArrayList;

import retrofit.mime.TypedFile;

/**
 * Created by David on 13.9.2014..
 */
public class FileUtil {

    private static ImageChooserManager imageChooserManager;
    private static int chooserType;
    private static String filePath;

    public static void pickFile(Fragment fragment) {
        // This always works
        // Intent i = new Intent(fragment.getActivity(), FilePickerActivity.class);
        // This works if you defined the intent filter
        Intent i = new Intent(Intent.ACTION_GET_CONTENT);
        // Set these depending on your use case. These are the defaults.
        i.putExtra(FilePickerActivity.EXTRA_ALLOW_MULTIPLE, false);
        i.putExtra(FilePickerActivity.EXTRA_ALLOW_CREATE_DIR, false);
        i.putExtra(FilePickerActivity.EXTRA_MODE, FilePickerActivity.MODE_FILE);
        fragment.startActivityForResult(i, Const.Request.FILE);
    }

    public static void pickImage(ImageChooserListener listener) {
        chooserType = ChooserType.REQUEST_PICK_PICTURE;
        imageChooserManager = new ImageChooserManager((Fragment) listener,
                ChooserType.REQUEST_PICK_PICTURE, "myfolder", true);
        imageChooserManager.setImageChooserListener(listener);
        try {
            filePath = imageChooserManager.choose();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void imageChooserActivityResult(ImageChooserListener listener, int resultCode, int requestCode, Intent data) {
        if (resultCode == Activity.RESULT_OK
                && (requestCode == ChooserType.REQUEST_PICK_PICTURE || requestCode == ChooserType.REQUEST_CAPTURE_PICTURE)) {
            if (imageChooserManager == null) {
                reinitializeImageChooser(listener);
            }
            imageChooserManager.submit(requestCode, data);
        }
    }

    public static Uri uriFromFilePickerResult(Context context, int requestCode, int resultCode, Intent data) {
        Uri uri = null;
        if (requestCode == Const.Request.FILE && resultCode == Activity.RESULT_OK) {
            if (data.getBooleanExtra(FilePickerActivity.EXTRA_ALLOW_MULTIPLE, false)) {
                // For JellyBean and above
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    ClipData clip = data.getClipData();

                    if (clip != null) {
                        for (int i = 0; i < clip.getItemCount(); i++) {
                            uri = clip.getItemAt(i).getUri();
                        }
                    }
                } else {
                    ArrayList<String> paths = data.getStringArrayListExtra
                            (FilePickerActivity.EXTRA_PATHS);
                    if (paths != null) {
                        for (String path : paths) {
                            uri = Uri.parse(path);
                        }
                    }
                }
            } else {
                uri = data.getData();
            }
        }
        return uri;
    }

    public static String getMimeType(String path) {
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(path);
        if (extension != null) {
            MimeTypeMap mime = MimeTypeMap.getSingleton();
            type = mime.getMimeTypeFromExtension(extension);
        }
        return type;
    }

    public static TypedFile typedFileFromPath(String path) {
        return new TypedFile(getMimeType(path), new File(path));
    }

    private static void reinitializeImageChooser(ImageChooserListener listener) {
        imageChooserManager = new ImageChooserManager((Fragment) listener, chooserType,
                "myfolder", true);
        imageChooserManager.setImageChooserListener(listener);
        imageChooserManager.reinitialize(filePath);
    }
}
