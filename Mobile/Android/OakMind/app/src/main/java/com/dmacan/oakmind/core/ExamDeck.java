package com.dmacan.oakmind.core;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;
import java.util.Random;

/**
 * Created by David on 31.8.2014..
 */
public class ExamDeck {

    private List<ExamCard> cards;

    public ExamDeck(List<ExamCard> cards) {
        this.cards = cards;
    }

    public ExamDeck() {
        this.cards = new ArrayList<ExamCard>();
    }

    /**
     * Shuffles the cards within the deck in random order
     */
    public void shuffle() {
        long seed = System.nanoTime();
        Collections.shuffle(this.cards, new Random(seed));
    }

    /**
     * Removes the given card from the deck
     *
     * @param card Card which is to be removed
     * @return true if the removal was successful, and vice-versa
     */
    public boolean removeCard(ExamCard card) {
        ListIterator<ExamCard> iterator = this.cards.listIterator();
        int i;
        while (iterator.hasNext()) {
            i = iterator.nextIndex();
            iterator.next();
            if (this.cards.get(i).getId() == card.getId()) {
                iterator.remove();
                return true;
            }
        }
        return false;
    }

    /**
     * Adds the given card to the deck
     *
     * @param card ExamCard which is to be added
     * @return true if the adding was successful, and vice-versa
     */
    public boolean addCard(ExamCard card) {
        return this.cards.add(card);
    }

    /**
     * Draws the card from the top (end) of the deck
     *
     * @return ExamCard drawn
     */
    public ExamCard drawLastCard() {
        return this.cards.get(this.cards.size() - 1);
    }

    /**
     * Draws the card from the beginning (start) of the deck
     *
     * @return ExamCard drawn
     */
    public ExamCard drawFirstCard() {
        return this.cards.get(0);
    }

    public List<ExamCard> getCards() {
        return cards;
    }

    public void setCards(List<ExamCard> cards) {
        this.cards = cards;
    }
}
