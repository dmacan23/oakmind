package com.dmacan.oakmind.data.controller;

import com.dmacan.oakmind.api.request.CreateRequestNew;
import com.dmacan.oakmind.api.request.DeleteRequest;
import com.dmacan.oakmind.api.request.ReadRequestNew;
import com.dmacan.oakmind.api.request.UpdateRequest;
import com.dmacan.oakmind.data.Deck;
import com.dmacan.oakmind.data.DeckCategory;
import com.dmacan.oakmind.data.User;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by David on 8.9.2014..
 */
public class DeckController extends DataController {

    private Callback<DeckCategory[]> readDeckCategoriesCallback = new Callback<DeckCategory[]>() {
        @Override
        public void success(DeckCategory[] deckCategories, Response response) {
            if (getOnDataMultipleReadListener() != null)
                getOnDataMultipleReadListener().onDataMultipleRead(deckCategories);
        }

        @Override
        public void failure(RetrofitError error) {
            Crouton.cancelAllCroutons();
            if (getOnNetworkErrorListener() != null)
                getOnNetworkErrorListener().onNetworkError(error);
        }
    };
    private Callback<Deck[]> readDecksCallback = new Callback<Deck[]>() {
        @Override
        public void success(Deck[] decks, Response response) {
            if (getOnDataMultipleReadListener() != null)
                getOnDataMultipleReadListener().onDataMultipleRead(decks);
        }

        @Override
        public void failure(RetrofitError error) {
            Crouton.cancelAllCroutons();
            if (getOnNetworkErrorListener() != null)
                getOnNetworkErrorListener().onNetworkError(error);
        }
    };

    public DeckController() {
        super();
    }

    public void readDecksPublic(boolean initBeforeExecution) {
        if (initBeforeExecution && getOnDataMultipleReadListener() != null)
            getOnDataMultipleReadListener().beforeExecution();
        ReadRequestNew request = new ReadRequestNew();
        request.setMethod("decksPublic");
        getOakMindAPI().readDecks(request, readDecksCallback);
    }

    public void readDecksPublic() {
        readDecksPublic(true);
    }

    public void readDecksOwned(User user, boolean initBeforeExecution) {
        if (initBeforeExecution && getOnDataMultipleReadListener() != null)
            getOnDataMultipleReadListener().beforeExecution();
        ReadRequestNew request = new ReadRequestNew();
        request.setMethod("decksOwned");
        request.setDataModel(user);
        getOakMindAPI().readDecks(request, readDecksCallback);
    }

    public void readDecksOwned(User user) {
        readDecksOwned(user, true);
    }

    public void createDeck(Deck deck, boolean initBeforeExecution) {
        if (initBeforeExecution && getOnDataCreatedListener() != null)
            getOnDataCreatedListener().beforeExecution();
        CreateRequestNew requestNew = new CreateRequestNew();
        requestNew.setMethod("deck");
        requestNew.setDataModel(deck);
        getOakMindAPI().createItem(requestNew, getCreateCallback());
    }

    public void createDeck(Deck deck) {
        createDeck(deck, true);
    }

    public void readCategories(boolean initBeforeExecution) {
        if (initBeforeExecution && getOnDataMultipleReadListener() != null)
            getOnDataMultipleReadListener().beforeExecution();
        ReadRequestNew request = new ReadRequestNew();
        request.setMethod("deckCategories");
        getOakMindAPI().readDeckCategories(request, readDeckCategoriesCallback);
    }

    public void readCategories() {
        readCategories(true);
    }

    public void deleteDeck(Deck deck, boolean initBeforeExecution) {
        if (initBeforeExecution && getOnDataDeletedListener() != null)
            getOnDataDeletedListener().beforeExecution();
        DeleteRequest request = new DeleteRequest();
        request.setMethod("deleteDeck");
        request.setDataModel(deck);
        getOakMindAPI().deleteData(request, getDeleteCallback());
    }

    public void deleteDeck(Deck deck) {
        deleteDeck(deck, true);
    }

    public void publishDeck(Deck deck, boolean initBeforeExecution) {
        if (initBeforeExecution && getOnDataUpdateListener() != null)
            getOnDataUpdateListener().beforeExecution();
        UpdateRequest request = new UpdateRequest();
        request.setMethod("publishDeck");
        Deck d = new Deck();
        d.setRemoteId(deck.getRemoteId());
        request.setDataModel(d);
        getOakMindAPI().updateData(request, getUpdateCallback());
    }

    public void publishDeck(Deck deck) {
        publishDeck(deck, true);
    }

    public void updateDeck(Deck deck, boolean initBeforeExecution) {
        if (initBeforeExecution && getOnDataUpdateListener() != null)
            getOnDataUpdateListener().beforeExecution();
        UpdateRequest request = new UpdateRequest();
        request.setMethod("updateDeck");
        request.setDataModel(deck);
        getOakMindAPI().updateData(request, getUpdateCallback());
    }

    public void updateDeck(Deck deck) {
        updateDeck(deck, true);
    }

}
