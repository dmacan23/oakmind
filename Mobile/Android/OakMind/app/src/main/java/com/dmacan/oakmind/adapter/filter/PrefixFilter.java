package com.dmacan.oakmind.adapter.filter;

import android.widget.BaseAdapter;
import android.widget.Filter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by David on 24.8.2014..
 */
public class PrefixFilter extends Filter {

    private boolean firstWordOnly;
    private List<FilterItem> items;
    private List<FilterItem> originalItems;
    private FilterResults results;
    private BaseAdapter adapter;

    public PrefixFilter(boolean firstWordOnly, List<FilterItem> items, BaseAdapter adapter) {
        this.firstWordOnly = firstWordOnly;
        this.items = items;
        this.adapter = adapter;
    }

    @Override
    public FilterResults performFiltering(CharSequence constraint) {
        results = new FilterResults();
        ArrayList<FilterItem> filteredArray = new ArrayList<FilterItem>();
        if (originalItems == null) {
            originalItems = new ArrayList<FilterItem>(items);
        }
        if (constraint == null || constraint.length() == 0) {
            results.count = originalItems.size();
            results.values = originalItems;
        } else {
            constraint = constraint.toString().toLowerCase();
            for (FilterItem item : originalItems) {
                isMatch(item, constraint, filteredArray);
            }
            results.count = filteredArray.size();
            results.values = filteredArray;
        }
        return results;
    }

    @Override
    public void publishResults(CharSequence constraint, FilterResults results) {
        items = (List<FilterItem>) results.values;
        adapter.notifyDataSetChanged();
    }

    private void isMatch(FilterItem item, CharSequence constraint, ArrayList<FilterItem> filteredArray) {
        String filterableString = item.filterableString().toLowerCase().trim();
        if (this.firstWordOnly)
            if (isPrefixMatch(filterableString, constraint.toString()))
                filteredArray.add(item);
            else {
                String[] filterable = filterableString.toString().split(" ");
                for (String s : filterable)
                    if (isPrefixMatch(s, constraint.toString())) {
                        filteredArray.add(item);
                        break;
                    }
            }
    }

    private boolean isPrefixMatch(String filterableString, String constraint) {
        return (filterableString.startsWith(constraint));
    }
}
