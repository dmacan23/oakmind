package com.dmacan.oakmind.fragment;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.dmacan.oakmind.DeckActivity;
import com.dmacan.oakmind.R;
import com.dmacan.oakmind.adapter.GridAdapter;
import com.dmacan.oakmind.data.Deck;
import com.dmacan.oakmind.util.APIUtil;
import com.dmacan.oakmind.util.Const;
import com.dmacan.oakmind.view.presenter.DeckPresenter;
import com.etsy.android.grid.StaggeredGridView;
import com.melnykov.fab.FloatingActionButton;
import com.nineoldandroids.animation.Animator;

import butterknife.InjectView;
import hr.mtlab.dmacan.magicui.graphics.MagicDrawable;

/**
 * Created by David on 6.9.2014..
 */
public abstract class DeckGridFragment extends OakMindFragment implements AdapterView.OnItemClickListener, Animator.AnimatorListener {

    @InjectView(R.id.grid)
    StaggeredGridView gridView;
    @InjectView(R.id.emptyContainerPlaceholder)
    TextView emptyContainer;
    @InjectView(R.id.btnFAB)
    FloatingActionButton fab;
    GridAdapter adapter;
    private Deck selectedDeck;

    @Override
    public int provideLayoutRes() {
        return R.layout.grid_layout;
    }

    @Override
    public void main() {
        adapter = new GridAdapter(getOakMindActivity());
//        adapter.enableSearch(getOakMindActivity(), getMenu(), true);
        adapter.setHeightModifier(0.4);
        gridView.setAdapter(adapter);
        gridView.setOnItemClickListener(this);
        gridView.setEmptyView(emptyContainer);
        loadData();
        fab.attachToListView(gridView);
        fab.setImageDrawable(new MagicDrawable(getOakMindActivity(), getResources().getString(R.string.fa_pen)).sizeDp(24).colorRes(R.color.white));
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        YoYo.with(Techniques.FadeOut).duration(200).playOn(view);
        YoYo.AnimationComposer fadeAnimation = YoYo.with(Techniques.FadeIn).duration(200).delay(210);
        selectedDeck = ((DeckPresenter) adapter.getItem(position)).getDeck();
        fadeAnimation.withListener(this);
        fadeAnimation.playOn(view);
    }

    private void openDeck() {
        Intent intent = new Intent(getOakMindActivity(), DeckActivity.class);
        boolean deckOwned = (this instanceof DecksOwnedFragment);
        intent.putExtra(Const.Key.DECK_OWNED, deckOwned);
        intent.putExtra(Const.Key.DECK_CURRENT, APIUtil.createGson().toJson(selectedDeck));
        startActivity(intent);
    }

    public abstract void loadData();

    @Override
    public void onAnimationEnd(Animator animator) {
        openDeck();
    }

    @Override
    public void onAnimationStart(Animator animator) {

    }

    @Override
    public void onAnimationRepeat(Animator animator) {

    }

    @Override
    public void onAnimationCancel(Animator animator) {

    }
}
