package com.dmacan.oakmind.api.listener;

import com.dmacan.oakmind.api.response.DeleteResponse;

/**
 * Created by David on 14.9.2014..
 */
public interface OnDataDeletedListener extends OnDataCRUDListener {
    public void onDataDeleted(DeleteResponse response);
}
