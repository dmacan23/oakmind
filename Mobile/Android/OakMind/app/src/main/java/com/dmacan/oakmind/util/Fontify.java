package com.dmacan.oakmind.util;

import android.graphics.Typeface;
import android.widget.TextView;

/**
 * Created by David on 17.8.2014..
 */
public class Fontify {

    public static void addFont(String fontName, TextView... textViews) {
        if (textViews.length > 0) {
            Typeface font = Typeface.createFromAsset(textViews[0].getContext()
                    .getAssets(), "fonts/" + fontName);
            for (TextView txt : textViews)
                txt.setTypeface(font);
        }
    }

    public static void addFont(TextView... textViews) {
        addFont(Font.FLAT_ICON, textViews);
    }

    public static class Font {
        public static final String FONT_AWESOME = "fontawesome.ttf";
        public static final String FLAT_ICON = "flaticon.ttf";
        public static final String OAKMIND_ICON = "oakmindicon.ttf";
        public static final String ANTONIO = "antonio.ttf";
    }
}
