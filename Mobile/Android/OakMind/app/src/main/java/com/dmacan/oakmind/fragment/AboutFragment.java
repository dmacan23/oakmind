package com.dmacan.oakmind.fragment;

import com.dmacan.oakmind.R;

/**
 * Created by David on 17.8.2014..
 */
public class AboutFragment extends OakMindFragment {

    @Override
    public int provideLayoutRes() {
        return R.layout.fragment_about;
    }

    @Override
    public void main() {

    }
}
