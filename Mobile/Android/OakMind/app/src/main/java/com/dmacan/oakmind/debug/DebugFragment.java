package com.dmacan.oakmind.debug;

import android.app.ProgressDialog;
import android.widget.Toast;

import com.dmacan.oakmind.R;
import com.dmacan.oakmind.api.listener.OnDataCreatedListener;
import com.dmacan.oakmind.api.listener.OnDataMultipleReadListener;
import com.dmacan.oakmind.api.listener.OnDataReadListener;
import com.dmacan.oakmind.api.listener.OnFileUploadListener;
import com.dmacan.oakmind.api.listener.OnNetworkErrorListener;
import com.dmacan.oakmind.api.response.CreateResponse;
import com.dmacan.oakmind.api.response.FileUploadResponse;
import com.dmacan.oakmind.data.DataModel;
import com.dmacan.oakmind.data.Deck;
import com.dmacan.oakmind.data.DeckCategory;
import com.dmacan.oakmind.data.Media;
import com.dmacan.oakmind.data.MediaType;
import com.dmacan.oakmind.data.User;
import com.dmacan.oakmind.data.controller.DeckController;
import com.dmacan.oakmind.data.controller.ExamController;
import com.dmacan.oakmind.fragment.OakMindFragment;
import com.dmacan.oakmind.widget.MagicColorPickerNew;
import com.squareup.picasso.Picasso;

import butterknife.InjectView;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit.RetrofitError;

/**
 * Created by David on 20.8.2014..
 */
public class DebugFragment extends OakMindFragment implements MagicColorPickerNew.OnColorSelectedListener
        , OnDataReadListener, OnNetworkErrorListener, OnDataMultipleReadListener, OnDataCreatedListener, OnFileUploadListener {

    @InjectView(R.id.imgUploaded)
    CircleImageView imgUploaded;
    ProgressDialog dialog;

    @Override
    public int provideLayoutRes() {
        return R.layout.fragment_debug;
    }

    @Override
    public void main() {
        ExamController controller = new ExamController();
        controller.setOnDataReadListener(this);
        controller.setOnNetworkErrorListener(this);
        controller.setOnDataMultipleReadListener(this);
//        controller.readDecksPublic();
        User u = new User();
        u.setRemoteId(47);
        Deck d = new Deck();
        d.setRemoteId(42);
        d.setUser(u);
        d.setLabel("Ovo je deck da se vidi radi li API");
        d.setDescription("Ovo je opis koji se ionako nigdje ne koristi");
        DeckCategory category = new DeckCategory();
        category.setRemoteId(1);
        d.setCategory(category);
        d.setColor("#ff0000");
        DeckController deckController = new DeckController();
        deckController.setOnDataCreatedListener(this);
        /*
        item.setColor(Color.parseColor("#aa22bb"));
        picker.setColors(generateColors());
        Where[] where = {
                new Where("username", "dmacan"),
                new Where("password", "xxx123")
        };
        ReadRequest request = new ReadRequest("users", where, true);
        Log.d("TESTIRANJE", OakMindController.createGson().toJson(request));
        UserController uc = new UserController(getOakMindActivity());
        uc.setOnUserReadListener(this);
        uc.readUser("dmacan", "xxx123");*/
    }

    @Override
    public void onDataRead(DataModel result) {
        Toast.makeText(getOakMindActivity(), "Uspjeh", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNetworkError(RetrofitError error) {
        Toast.makeText(getOakMindActivity(), "Neuspjeh", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDataMultipleRead(DataModel[] result) {
        Toast.makeText(getOakMindActivity(), "Višestruki uspjeh", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDataCreated(CreateResponse response) {
        if (response.getMethod().equals("card")) {
            Toast.makeText(getOakMindActivity(), "Card has been created with id: " + response.getId(), Toast.LENGTH_SHORT).show();
        } else if (response.getMethod().equals("deck")) {
            Toast.makeText(getOakMindActivity(), "Deck has been created with id: " + response.getId(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onFileUploaded(FileUploadResponse response) {
        Media media = new Media();
        media.setLocation(response.getFilePath());
        media.setRemoteId(response.getId());
        MediaType mt = new MediaType();
        mt.setRemoteId(1);
        media.setMediaType(mt);
        Picasso.with(getOakMindActivity()).load(media.getLocation()).noFade().into(imgUploaded);
        dialog.dismiss();
    }

    @Override
    public void beforeExecution() {
        if (dialog == null)
            dialog = new ProgressDialog(getOakMindActivity());
        dialog.setTitle("Uploading photo");
        dialog.show();
    }

    @Override
    public void onColorSelected(int color) {
        Toast.makeText(getOakMindActivity(), "Odabrao si boju!", Toast.LENGTH_SHORT).show();
    }
}
