package com.dmacan.oakmind;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.dmacan.oakmind.content.Session;
import com.dmacan.oakmind.data.User;
import com.dmacan.oakmind.fragment.AboutFragment;
import com.dmacan.oakmind.fragment.DecksBrowseFragment;
import com.dmacan.oakmind.fragment.DecksOwnedFragment;
import com.dmacan.oakmind.fragment.ProgressFragment;
import com.dmacan.oakmind.fragment.SettingsFragment;
import com.dmacan.oakmind.widget.drawer.DrawerData;
import com.dmacan.oakmind.widget.drawer.DrawerItem;
import com.dmacan.oakmind.widget.drawer.DrawerItemProfile;
import com.dmacan.oakmind.widget.drawer.OakMindDrawerActivity;

import net.simonvt.menudrawer.MenuDrawer;
import net.simonvt.menudrawer.Position;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by David on 17.8.2014..
 */
public class MainActivity extends OakMindDrawerActivity {

    @Override
    protected int getDragMode() {
        return MenuDrawer.MENU_DRAG_WINDOW;
    }

    @Override
    protected Position getDrawerPosition() {
        return Position.LEFT;
    }

    @Override
    public int provideLayoutRes() {
        return R.layout.activity_main;
    }

    @Override
    public void main(Bundle savedInstanceState, boolean isDrawerActivity) {
    }

    @Override
    public List<DrawerItem> provideDrawerItems() {
        List<DrawerItem> items = new ArrayList<DrawerItem>();
        DrawerData.context = getBaseContext();
        User user = Session.readCurrentUserNew(getBaseContext());
        // TODO promijeniti
        items.add(new DrawerItemProfile(user.getUsername(), user.getAvatar().getLocation()));
        items.add(DrawerData.newItem(R.string.fi_progress, R.string.di_item_profile_progress));
        items.add(DrawerData.newItem(R.string.fi_logout, R.string.di_item_profile_logout));
        items.add(DrawerData.newCategory(R.string.di_category_decks));
        items.add(DrawerData.newItem(R.string.fi_deck_unsorted, R.string.di_item_deck_own));
        items.add(DrawerData.newItem(R.string.fi_deck_browse, R.string.di_item_deck_browse));
        items.add(DrawerData.newCategory(R.string.di_category_oakmind));
//        items.add(DrawerData.newItem(R.string.fi_settings, R.string.di_item_oakmind_settings));
        items.add(DrawerData.newItem(R.string.fi_info, R.string.di_item_oakmind_info));
        return items;
    }

    // TODO: rename method
    @Override
    public Fragment provideFragment(String tag) {
        if (tag.equals(getOakMindString(R.string.di_item_deck_browse)) || tag.equals(DrawerData.DEFAULT_TAG))
            return new DecksBrowseFragment();
        else if (tag.equals(getOakMindString(R.string.di_item_deck_own)))
            return new DecksOwnedFragment();
        else if (tag.equals(getOakMindString(R.string.di_item_profile_progress)))
            return new ProgressFragment();
        else if (tag.equals(getOakMindString(R.string.di_item_oakmind_info)))
            return new AboutFragment();
        else if (tag.equals(getOakMindString(R.string.di_item_oakmind_settings)))
            return new SettingsFragment();
        else if (tag.equals(getOakMindString(R.string.di_item_profile_logout))) {
            Session.deleteCurrentUser(getBaseContext());
            startActivity(new Intent(getBaseContext(), LoginActivity.class));
            this.finish();
        }
        return null;
    }


}
