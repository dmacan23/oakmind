package com.dmacan.oakmind.api.listener;

import com.dmacan.oakmind.api.response.UpdateResponse;

/**
 * Created by David on 15.9.2014..
 */
public interface OnDataUpdateListener extends OnDataCRUDListener {

    public void onDataUpdate(UpdateResponse response);

}
