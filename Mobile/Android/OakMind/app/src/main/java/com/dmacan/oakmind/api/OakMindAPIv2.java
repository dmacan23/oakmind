package com.dmacan.oakmind.api;

import com.dmacan.oakmind.api.request.CreateRequestNew;
import com.dmacan.oakmind.api.request.DeleteRequest;
import com.dmacan.oakmind.api.request.ReadRequestNew;
import com.dmacan.oakmind.api.request.UpdateRequest;
import com.dmacan.oakmind.api.response.CreateResponse;
import com.dmacan.oakmind.api.response.DeleteResponse;
import com.dmacan.oakmind.api.response.FileUploadResponse;
import com.dmacan.oakmind.api.response.UpdateResponse;
import com.dmacan.oakmind.data.Card;
import com.dmacan.oakmind.data.Deck;
import com.dmacan.oakmind.data.DeckCategory;
import com.dmacan.oakmind.data.Exam;
import com.dmacan.oakmind.data.User;
import com.dmacan.oakmind.util.Const;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.mime.TypedFile;

/**
 * Created by David on 16.8.2014..
 */
public interface OakMindAPIv2 {

    @POST(Const.API.DATA_READ)
    void readUser(@Body ReadRequestNew request, Callback<User> callback);

    @POST(Const.API.DATA_READ)
    void readDecks(@Body ReadRequestNew request, Callback<Deck[]> callback);

    @POST(Const.API.DATA_READ)
    void readCards(@Body ReadRequestNew request, Callback<Card[]> callback);

    @POST(Const.API.DATA_READ)
    void readExams(@Body ReadRequestNew request, Callback<Exam[]> callback);

    @POST(Const.API.DATA_READ)
    void readDeckCategories(@Body ReadRequestNew request, Callback<DeckCategory[]> callback);

    @POST("/data_create.php")
    void createItem(@Body CreateRequestNew request, Callback<CreateResponse> callback);

    @Multipart
    @POST("/data_upload.php")
    void uploadFile(@Part("file") TypedFile file, Callback<FileUploadResponse> callback);

    @POST("/data_update.php")
    void updateData(@Body UpdateRequest request, Callback<UpdateResponse> callback);

    @POST("/data_update.php")
    void deleteData(@Body DeleteRequest request, Callback<DeleteResponse> callback);
}
