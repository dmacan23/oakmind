package com.dmacan.oakmind.content;

import android.content.Context;

import com.dmacan.oakmind.data.User;
import com.dmacan.oakmind.util.APIUtil;
import com.dmacan.oakmind.util.Const;

import vee.android.lib.SimpleSharedPreferences;

/**
 * Created by David on 18.8.2014..
 */
public class Session {

    public static void saveCurrentUserNew(Context context, User user) {
        new SimpleSharedPreferences(context).putString(Const.Key.USER_CURRENT, APIUtil.createGson().toJson(user));
    }

    public static User readCurrentUserNew(Context context) {
        return APIUtil.createGson().fromJson(new SimpleSharedPreferences(context).getString(Const.Key.USER_CURRENT, null), User.class);
    }

    public static void deleteCurrentUser(Context context) {
        new SimpleSharedPreferences(context).clear();
    }

}
