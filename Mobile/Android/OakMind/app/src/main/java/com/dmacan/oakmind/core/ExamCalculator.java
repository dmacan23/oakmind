package com.dmacan.oakmind.core;

/**
 * Created by David on 7.9.2014..
 */
public class ExamCalculator {

    // Maximum number of points
    private static final int POINT_MODIFIER = 100;
    // Multiplication of maximum number of points if all the answers were correct
    private static final int PERFECT_SCORE_MODIFIER = 2;

    private int correctAnswers = 0;
    private int incorrectAnswers = 0;

    /**
     * Calculates the points earned during the exam with the formula: (% of correct answers * POINT_MODIFIER)
     * If there were no incorrect answers, point number formula is: POINT_MODIFIER * PERFECT_SCORE_MODIFIER
     *
     * @return result
     */
    public double calculatePoints() {
        if (incorrectAnswers == 0)
            return POINT_MODIFIER * PERFECT_SCORE_MODIFIER;
        else
            return (((double) correctAnswers) / ((double) (correctAnswers + incorrectAnswers))) * POINT_MODIFIER;
    }

    /**
     * Increases the correct answers counter
     */
    public void addCorrect() {
        ++correctAnswers;
    }

    /**
     * Increases the incorrect answers counter
     */
    public void addIncorrect() {
        ++incorrectAnswers;
    }

}
