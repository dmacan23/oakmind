package com.dmacan.oakmind.fragment;

import android.content.Intent;

import com.dmacan.oakmind.DeckCreateActivity;
import com.dmacan.oakmind.R;
import com.dmacan.oakmind.api.listener.OnDataMultipleReadListener;
import com.dmacan.oakmind.api.listener.OnNetworkErrorListener;
import com.dmacan.oakmind.content.Session;
import com.dmacan.oakmind.data.DataModel;
import com.dmacan.oakmind.data.Deck;
import com.dmacan.oakmind.data.controller.DeckController;
import com.dmacan.oakmind.view.presenter.DeckPresenter;

import butterknife.OnClick;
import retrofit.RetrofitError;

/**
 * Created by David on 17.8.2014..
 */
public class DecksOwnedFragment extends DeckGridFragment implements OnDataMultipleReadListener, OnNetworkErrorListener {

    private DeckController deckController;

    @Override
    public void loadData() {
        setTitle(R.string.di_item_deck_own);
        deckController = new DeckController();
        deckController.setOnDataMultipleReadListener(this);
        deckController.setOnNetworkErrorListener(this);
    }

    @OnClick(R.id.btnFAB)
    void createDeck() {
        startActivity(new Intent(getOakMindActivity(), DeckCreateActivity.class));
    }

    @Override
    public void onDataMultipleRead(DataModel[] result) {
        adapter.empty();
        for (DataModel d : result) {
            Deck deck = (Deck) d;
            adapter.addItem(new DeckPresenter(deck));
        }
        messenger().showSuccess("Decks loaded");
    }

    @Override
    public void beforeExecution() {
        messenger().showLoading("Loading your decks");
    }

    @Override
    public void onResume() {
        super.onResume();
        deckController.readDecksOwned(Session.readCurrentUserNew(getOakMindActivity()));
    }

    @Override
    public void onNetworkError(RetrofitError error) {
        adapter.empty();
    }
}
