package com.dmacan.oakmind.data.controller;

import com.dmacan.oakmind.api.request.CreateRequestNew;
import com.dmacan.oakmind.api.request.ReadRequestNew;
import com.dmacan.oakmind.data.Card;
import com.dmacan.oakmind.data.Deck;
import com.dmacan.oakmind.data.Exam;
import com.dmacan.oakmind.data.User;

import java.util.ArrayList;
import java.util.List;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by David on 8.9.2014..
 */
public class ExamController extends DataController {
    private Callback<Exam[]> readExamsCallback = new Callback<Exam[]>() {
        @Override
        public void success(Exam[] exams, Response response) {
            if (getOnDataMultipleReadListener() != null)
                getOnDataMultipleReadListener().onDataMultipleRead(exams);
        }

        @Override
        public void failure(RetrofitError error) {
            Crouton.cancelAllCroutons();
            if (getOnNetworkErrorListener() != null)
                getOnNetworkErrorListener().onNetworkError(error);
        }
    };

    public ExamController() {
        super();
    }

    public void readExamsForDeck(Deck d, boolean initBeforeExecution) {
        if (initBeforeExecution && getOnDataMultipleReadListener() != null)
            getOnDataMultipleReadListener().beforeExecution();
        ReadRequestNew request = new ReadRequestNew();
        request.setMethod("examsForDeck");
        request.setDataModel(d);
        getOakMindAPI().readExams(request, readExamsCallback);
    }

    public void readExamsForDeck(Deck d) {
        readExamsForDeck(d, true);
    }


    public void readExamsForUser(User user, boolean initBeforeExecution) {
        if (initBeforeExecution && getOnDataMultipleReadListener() != null)
            getOnDataMultipleReadListener().beforeExecution();
        ReadRequestNew request = new ReadRequestNew();
        request.setMethod("examsForUser");
        request.setDataModel(user);
        getOakMindAPI().readExams(request, readExamsCallback);
    }

    public void readExamsForUser(User user) {
        readExamsForUser(user, true);
    }

    public void createExam(Exam exam) {
        createExam(exam, true);
    }

    public void createExam(Exam exam, boolean initBeforeExecution) {
        if (initBeforeExecution && (getOnDataCreatedListener() != null))
            getOnDataCreatedListener().beforeExecution();
        CreateRequestNew request = new CreateRequestNew();
        List<Card> cards = new ArrayList<Card>(exam.getDeck().getCards());
        exam.getDeck().setCards(null);
        request.setDataModel(exam);
        request.setMethod("examRecord");
        getOakMindAPI().createItem(request, getCreateCallback());
        exam.getDeck().setCards(cards);
    }
}
