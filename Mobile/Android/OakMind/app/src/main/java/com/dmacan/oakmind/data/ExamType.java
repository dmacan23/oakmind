package com.dmacan.oakmind.data;

import com.google.gson.annotations.Expose;

/**
 * Created by David on 8.9.2014..
 */
public class ExamType extends DataModel {

    @Expose
    private String label;

    public ExamType() {
        super();
    }

    public ExamType(long remoteId) {
        setRemoteId(remoteId);
    }

    public ExamType(String label) {
        super();
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
