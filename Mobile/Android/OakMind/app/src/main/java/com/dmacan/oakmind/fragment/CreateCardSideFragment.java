package com.dmacan.oakmind.fragment;


import com.dmacan.oakmind.R;
import com.dmacan.oakmind.view.PagerFragment;

import java.util.Locale;

import butterknife.InjectView;
import hr.mtlab.dmacan.magicui.widget.MagicEditText;

/**
 * Created by David on 27.8.2014..
 */
public class CreateCardSideFragment extends OakMindFragment implements PagerFragment {

    @InjectView(R.id.cardSide)
    MagicEditText etSide;

    private String label;

    @Override
    public int provideLayoutRes() {
        return R.layout.fragment_card_side_create;
    }

    @Override
    public void main() {
        etSide.setHint("Your " + label.toLowerCase(Locale.ENGLISH));
    }

    @Override
    public String provideLabel() {
        return this.label;
    }

    @Override
    public void setLabel(String label) {
        this.label = label;
    }

    public String getValue() {
        return etSide.getText().toString();
    }

}
