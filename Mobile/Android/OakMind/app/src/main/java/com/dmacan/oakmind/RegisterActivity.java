package com.dmacan.oakmind;

import android.os.Bundle;

import com.dmacan.oakmind.fragment.RegisterFragment;

/**
 * Created by David on 16.8.2014..
 */
public class RegisterActivity extends OakMindActivity {

    @Override
    public int provideLayoutRes() {
        return R.layout.activity_container;
    }

    @Override
    public void main(Bundle savedInstanceState) {
        setupFragment(R.id.container, new RegisterFragment());
    }
}
