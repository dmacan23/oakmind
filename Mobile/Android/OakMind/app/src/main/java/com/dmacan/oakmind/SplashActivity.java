package com.dmacan.oakmind;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.dmacan.oakmind.content.Session;
import com.dmacan.oakmind.util.Const;

/**
 * Created by David on 15.9.2014..
 */
public class SplashActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        main(savedInstanceState);
    }

    public void main(Bundle savedInstanceState) {
        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                if (Session.readCurrentUserNew(getBaseContext()) != null) {
                    startActivity(new Intent(getBaseContext(), MainActivity.class));
                } else
                    startActivity(new Intent(getBaseContext(), LoginActivity.class));
                // close this activity
                finish();
            }
        }, Const.Duration.SPLASH);
    }
}
