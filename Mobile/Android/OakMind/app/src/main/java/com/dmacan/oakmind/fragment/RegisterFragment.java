package com.dmacan.oakmind.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;

import com.dmacan.oakmind.MainActivity;
import com.dmacan.oakmind.R;
import com.dmacan.oakmind.api.listener.OnDataCreatedListener;
import com.dmacan.oakmind.api.listener.OnFileUploadListener;
import com.dmacan.oakmind.api.listener.OnItemCreatedListener;
import com.dmacan.oakmind.api.response.CreateResponse;
import com.dmacan.oakmind.api.response.FileUploadResponse;
import com.dmacan.oakmind.content.Session;
import com.dmacan.oakmind.data.controller.MediaController;
import com.dmacan.oakmind.data.controller.UserController;
import com.dmacan.oakmind.util.FileUtil;
import com.dmacan.oakmind.util.FormValidator;
import com.dmacan.oakmind.widget.MagicMessenger;
import com.joanzapata.android.iconify.Iconify;
import com.kbeanie.imagechooser.api.ChosenImage;
import com.kbeanie.imagechooser.api.ImageChooserListener;

import java.io.File;

import butterknife.InjectView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit.mime.TypedFile;

/**
 * Created by David on 10.8.2014..
 */
public class RegisterFragment extends OakMindFragment implements OnItemCreatedListener, OnFileUploadListener, OnDataCreatedListener, ImageChooserListener {

    @InjectView(R.id.txtIconRegUsername)
    TextView iconUsername;
    @InjectView(R.id.txtIconRegEmail)
    TextView iconEmail;
    @InjectView(R.id.txtIconRegPassword)
    TextView iconPassword;
    @InjectView(R.id.txtIconRegPasswordConfirm)
    TextView iconPasswordConfirm;
    @InjectView(R.id.imgRegAvatar)
    CircleImageView imgAvatar;
    @InjectView(R.id.etRegUsername)
    EditText etUsername;
    @InjectView(R.id.etRegEmail)
    EditText etEmail;
    @InjectView(R.id.etRegPassword)
    EditText etPassword;
    @InjectView(R.id.etRegPasswordConfirm)
    EditText etPasswordConfirm;

    private com.dmacan.oakmind.data.controller.UserController userController;
    private TypedFile profileImage;
    private ProgressDialog dialog;
    private MediaController mediaController;
    private com.dmacan.oakmind.data.User user;

    @Override
    public int provideLayoutRes() {
        return R.layout.fragment_register;
    }

    @Override
    public void main() {
        setTitle(R.string.label_register);
        Iconify.addIcons(iconUsername, iconEmail, iconPassword, iconPasswordConfirm, etEmail, etPassword, etPasswordConfirm, etUsername);
    }

    @OnClick(R.id.btnRegister)
    public void register() {
        if (mediaController != null && validate()) {
            mediaController.setOnDataCreatedListener(this);
            mediaController.setOnFileUploadListener(this);
            mediaController.uploadImage(profileImage);
        }
    }

    public boolean validate() {
        boolean pass = true;
        if (FormValidator.isEmpty(etUsername)) {
            pass = false;
            messenger().showError(getString(R.string.sm_reg_username_empty));
            etUsername.requestFocus();
        }
        if (FormValidator.isEmpty(etPassword)) {
            pass = false;
            messenger().showError(getString(R.string.sm_reg_pass_empty));
            etPassword.requestFocus();
        }
        if (FormValidator.isEmpty(etEmail)) {
            pass = false;
            messenger().showError(getString(R.string.sm_reg_email_empty));
            etEmail.requestFocus();
        }
        if (!FormValidator.isEmailValid(etEmail.getText().toString())) {
            pass = false;
            messenger().showError(getString(R.string.sm_reg_email_format_incorrect));
            etEmail.requestFocus();
        }
        if (!FormValidator.isPasswordMatch(etPassword.getText().toString(), etPasswordConfirm.getText().toString())) {
            pass = false;
            messenger().showError(R.string.sm_reg_pass_mismatch);
            etPasswordConfirm.requestFocus();
        }
        if (!FormValidator.isUsernameValid(etUsername.getText().toString())) {
            pass = false;
            messenger().showError(getString(R.string.sm_reg_username_invalid));
            etUsername.requestFocus();
        }
        return pass;
    }

    @Override
    public void onItemCreated(CreateResponse response) {
        new MagicMessenger(getOakMindActivity()).showSuccess(response.getMessage());
    }

    @OnClick(R.id.imgRegAvatar)
    void pickImage() {
        FileUtil.pickImage(this);
    }

    @Override
    public void onFileUploaded(FileUploadResponse response) {
        com.dmacan.oakmind.data.Media profilePicture = new com.dmacan.oakmind.data.Media();
        profilePicture.setLocation(response.getFilePath());
        profilePicture.setRemoteId(response.getId());
        profilePicture.setLabel(etUsername.getText().toString() + "_profile_pic");
        user = new com.dmacan.oakmind.data.User();
        user.setUsername(etUsername.getText().toString());
        user.setPassword(etPassword.getText().toString());
        user.setEmail(etEmail.getText().toString());
        user.setAvatar(profilePicture);
        userController = new UserController();
        userController.setOnDataCreatedListener(this);
        userController.createUser(getOakMindActivity(), user, false);
    }

    @Override
    public void beforeExecution() {
        messenger().showLoading("Registration in process");
    }

    @Override
    public void onDataCreated(CreateResponse response) {
        messenger().dismissAll();
        if (response.getMethod().equals("user") && response.getId() > 0) {
            user.setRemoteId(response.getId());
            Session.saveCurrentUserNew(getOakMindActivity(), user);
            startActivity(new Intent(getOakMindActivity(), MainActivity.class));
            getOakMindActivity().finish();
        }
    }

    @Override
    public void onImageChosen(final ChosenImage chosenImage) {
        getOakMindActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Uri imageUri = Uri.parse(new File(chosenImage.getFileThumbnail()).toString());
                profileImage = FileUtil.typedFileFromPath(imageUri.getPath());
                mediaController = new MediaController();
                imgAvatar.setImageDrawable(Drawable.createFromPath(imageUri.getPath()));
            }
        });
    }

    @Override
    public void onError(final String s) {
        getOakMindActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Log.d("IMAGECHOOSER", "Error: " + s);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        FileUtil.imageChooserActivityResult(this, resultCode, requestCode, data);
    }

}
