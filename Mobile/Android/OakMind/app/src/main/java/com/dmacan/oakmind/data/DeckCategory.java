package com.dmacan.oakmind.data;

import com.google.gson.annotations.Expose;

/**
 * Created by David on 8.9.2014..
 */
public class DeckCategory extends DataModel {

    @Expose
    private String label;
    @Expose
    private String symbol;

    public DeckCategory() {
        super();
    }

    public DeckCategory(String label, String symbol) {
        super();
        this.label = label;
        this.symbol = symbol;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }
}
