package com.dmacan.oakmind.adapter;

import android.content.Context;
import android.os.Handler;
import android.support.v7.widget.SearchView;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.dmacan.oakmind.OakMindActivity;
import com.dmacan.oakmind.R;
import com.dmacan.oakmind.adapter.filter.FilterItem;
import com.dmacan.oakmind.view.GridItem;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import hr.mtlab.dmacan.magicui.graphics.MagicDrawable;

/**
 * Created by David on 24.8.2014..
 */
public class GridAdapter extends BaseAdapter implements Filterable {

    private static final SparseArray<Double> sPositionHeightRatios = new SparseArray<Double>();
    private List<GridItem> items;
    private Context context;
    private boolean filterFirstWordOnly;
    private Random random;
    private LayoutInflater inflater;
    private boolean animationEnabled = false;
    private Techniques techniques = Techniques.BounceIn;
    private double heightModifier = 1.0;

    // Filter stuff
    private boolean firstWordOnly;
    private List<FilterItem> filterItems;
    private List<FilterItem> originalItems;
    private BaseAdapter adapter;

    public GridAdapter(Context context, List<GridItem> items) {
        this.items = items;
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.random = new Random();
    }

    public GridAdapter(Context context) {
        this(context, new ArrayList<GridItem>());
    }

    public GridAdapter(Context context, int heightModifier) {
        this(context);
        this.heightModifier = heightModifier;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public GridItem getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void addItem(GridItem item) {
        this.items.add(item);
        this.notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflater.inflate(items.get(position).provideItemLayoutRes(), parent, false);
        items.get(position).display(convertView, position, getPositionRatio(position));
        if (this.animationEnabled) {
            YoYo.with(techniques).duration(1000).playOn(convertView);
        }
        return convertView;
    }

    private double getPositionRatio(final int position) {
        double ratio = sPositionHeightRatios.get(position, 0.0);
        // if not yet done generate and stash the columns height
        // in our real world scenario this will be determined by
        // some match based on the known height and width of the image
        // and maybe a helpful way to get the column height!
        if (ratio == 0) {
            ratio = getRandomHeightRatio();
            sPositionHeightRatios.append(position, ratio);
        }
        return ratio;
    }

    public void setHeightModifier(double heightModifier) {
        this.heightModifier = heightModifier;
    }

    private double getRandomHeightRatio() {
        return (random.nextDouble() / 2.0) + heightModifier; // height will be 1.0 - 1.5 the width
    }

    public void enableSearch(final OakMindActivity activity, Menu menu, boolean firstWordOnly) {
        this.filterFirstWordOnly = firstWordOnly;
        setupSearch(menu, activity);
    }

    @Override
    public Filter getFilter() {
        // return new PrefixFilter(filterFirstWordOnly, (List<FilterItem>) (List<?>) items, this);
        return new Filter() {
            @Override
            public FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                ArrayList<FilterItem> filteredArray = new ArrayList<FilterItem>();
                if (originalItems == null) {
                    originalItems = new ArrayList<FilterItem>(items);
                }
                if (constraint == null || constraint.length() == 0) {
                    results.count = originalItems.size();
                    results.values = originalItems;
                } else {
                    constraint = constraint.toString().toLowerCase();
                    for (FilterItem item : originalItems) {
                        isMatch(item, constraint, filteredArray);
                    }
                    results.count = filteredArray.size();
                    results.values = filteredArray;
                }
                return results;
            }

            @Override
            public void publishResults(CharSequence constraint, FilterResults results) {
                items = (List<GridItem>) results.values;
                GridAdapter.this.notifyDataSetChanged();
            }

            private void isMatch(FilterItem item, CharSequence constraint, ArrayList<FilterItem> filteredArray) {
                String filterableString = item.filterableString().toLowerCase().trim();
                if (isPrefixMatch(filterableString, String.valueOf(constraint)))
                    filteredArray.add(item);
               /* if (firstWordOnly)
                    if (isPrefixMatch(filterableString, constraint.toString()))
                        filteredArray.add(item);
                    else {
                        String[] filterable = filterableString.toString().split(" ");
                        for (String s : filterable)
                            if (isPrefixMatch(s, constraint.toString())) {
                                filteredArray.add(item);
                                break;
                            }
                    }*/
            }

            private boolean isPrefixMatch(String filterableString, String constraint) {
                return (filterableString.startsWith(constraint));
            }
        };
    }

    // TODO doraditi
    private void setupSearch(Menu menu, final OakMindActivity activity) {
        // Implementing ActionBar Search inside a fragment
        MenuItem item = menu.add("Search");
        MagicDrawable icon = new MagicDrawable(activity, activity.getResources().getString(R.string.fa_spyglass));
        icon.sizeDp(20);
        icon.colorRes(R.color.white);
        item.setIcon(icon); // sets icon
        item.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        final SearchView sv = new SearchView(activity);

        // implementing the listener
        sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(sv.getWindowToken(), 0);

                activity.setSupportProgressBarIndeterminateVisibility(true);

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        activity.setSupportProgressBarIndeterminateVisibility(false);
                    }
                }, 2000);

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.length() > 0) {
                    GridAdapter.this.getFilter().filter(newText);
                } else {
                    GridAdapter.this.getFilter().filter("");
                }
                return false;
            }
        });
        item.setActionView(sv);
    }
  /*  @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void setupSearch(Menu menu, final OakMindActivity activity) {
        final SearchView searchView = new SearchView(activity.getSupportActionBar().getThemedContext());
        activity.getSupportActionBar().setIcon(null);
        searchView.setQueryHint("Search");
        MagicDrawable icon = new MagicDrawable(activity, activity.getResources().getString(R.string.fa_spyglass));
        icon.sizeDp(20);
        icon.colorRes(R.color.white);
        menu.add(Menu.NONE, Menu.NONE, 1, "Search")
                .setIcon(icon)
                .setActionView(searchView)
                .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS | MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.length() > 0) {
                    GridAdapter.this.getFilter().filter(newText);
                } else {
                    GridAdapter.this.getFilter().filter("");
                }
                return false;
            }

            @Override
            public boolean onQueryTextSubmit(String query) {

                InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(searchView.getWindowToken(), 0);

                activity.setSupportProgressBarIndeterminateVisibility(true);

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        activity.setSupportProgressBarIndeterminateVisibility(false);
                    }
                }, 2000);

                return false;
            }
        });
    }*/

    public boolean isAnimationEnabled() {
        return animationEnabled;
    }

    public void setAnimationEnabled(boolean animationEnabled) {
        this.animationEnabled = animationEnabled;
    }

    public Techniques getTechniques() {
        return techniques;
    }

    public void setTechniques(Techniques techniques) {
        this.techniques = techniques;
    }

    public void empty() {
        this.items.clear();
        notifyDataSetChanged();
        notifyDataSetInvalidated();
    }
}
