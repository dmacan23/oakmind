package com.dmacan.oakmind.view.presenter;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dmacan.oakmind.R;
import com.dmacan.oakmind.data.Card;
import com.dmacan.oakmind.view.GridItem;
import com.etsy.android.grid.util.DynamicHeightTextView;

/**
 * Created by David on 7.9.2014..
 */
public class CardPresenter implements GridItem {

    private Card card;
    private String identifier = "Q";
    private double height = 0;

    public CardPresenter(Card card) {
        this.card = card;
    }

    @Override
    public void display(View view, int position, double dynamicHeight) {
        if (dynamicHeight != 0)
            this.height = dynamicHeight;
        else
            dynamicHeight = this.height;
        TextView txtLabel = (TextView) view.findViewById(R.id.giTitle);
        DynamicHeightTextView txtIcon = (DynamicHeightTextView) view.findViewById(R.id.giSymbol);
        LinearLayout background = (LinearLayout) view.findViewById(R.id.giLayout);
        txtLabel.setText(this.identifier);
        if (this.identifier.equals("Q")) {
            txtIcon.setText(card.getQuestion().getTitle());
        } else {
            txtIcon.setText(card.getAnswer().getTitle());
        }
        txtIcon.setHeightRatio(dynamicHeight);
    }

    @Override
    public int provideItemLayoutRes() {
        return R.layout.grid_item_card;
    }

    @Override
    public String filterableString() {
        return card.getQuestion().getTitle();
    }

    public void toggleQA() {
        if (this.identifier.equals("Q"))
            this.identifier = "A";
        else
            this.identifier = "Q";
    }
}
