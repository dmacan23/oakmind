package com.dmacan.oakmind.widget.drawer;

/**
 * Created by David on 17.8.2014..
 */
public class DrawerItemSimple extends DrawerItem {

    private String icon;

    public DrawerItemSimple(String icon, String label) {
        super(label);
        this.icon = icon;
    }

    public DrawerItemSimple(String icon, String label, boolean fragment) {
        super(label, fragment);
        this.icon = icon;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}
