package com.dmacan.oakmind.fragment;

import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;

import com.dmacan.oakmind.R;
import com.dmacan.oakmind.adapter.TabFragmentAdapter;
import com.dmacan.oakmind.api.listener.OnDataCreatedListener;
import com.dmacan.oakmind.api.response.CreateResponse;
import com.dmacan.oakmind.data.Answer;
import com.dmacan.oakmind.data.Card;
import com.dmacan.oakmind.data.Deck;
import com.dmacan.oakmind.data.Question;
import com.dmacan.oakmind.data.controller.CardController;
import com.dmacan.oakmind.util.APIUtil;
import com.dmacan.oakmind.util.Const;
import com.viewpagerindicator.TabPageIndicator;

import java.util.ArrayList;
import java.util.List;

import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by David on 23.8.2014..
 */
public class CardCreateFragment extends OakMindFragment implements OnDataCreatedListener {

    @InjectView(R.id.pager)
    ViewPager pager;
    @InjectView(R.id.indicator)
    TabPageIndicator tabIndicator;
    private TabFragmentAdapter adapter;
    private List<Fragment> fragmentPages;
    private Deck deck;
    private CardController cardController;

    @Override
    public int provideLayoutRes() {
        return R.layout.fragment_card_create;
    }

    @Override
    public void main() {
        initFragmentPages();
        this.cardController = new CardController();
        this.deck = APIUtil.createGson().fromJson(getOakMindActivity().getIntent().getExtras().getString(Const.Key.DECK_CURRENT), Deck.class);
        this.adapter = new TabFragmentAdapter(getChildFragmentManager(), fragmentPages);
        this.pager.setAdapter(adapter);
        this.tabIndicator.setViewPager(pager);
    }

    // TODO translate
    private void initFragmentPages() {
        this.fragmentPages = new ArrayList<Fragment>();
        CreateCardSideFragment question = new CreateCardSideFragment();
        question.setLabel("Question");
        CreateCardSideFragment answer = new CreateCardSideFragment();
        answer.setLabel("Answer");
        fragmentPages.add(question);
        fragmentPages.add(answer);
    }

    @OnClick(R.id.btnCreateCard)
    void createCard() {
        Question question = new Question();
        question.setTitle(((CreateCardSideFragment) fragmentPages.get(0)).getValue());
        Answer answer = new Answer();
        answer.setTitle(((CreateCardSideFragment) fragmentPages.get(1)).getValue());
        Card card = new Card();
        card.setAnswer(answer);
        card.setQuestion(question);
        card.setDeck(this.deck);
        CardController controller = new CardController();
        controller.setOnDataCreatedListener(this);
        controller.createCard(card);
    }

    @Override
    public void onDataCreated(CreateResponse response) {
        messenger().dismissAll();
        this.getOakMindActivity().finish();
    }

    @Override
    public void beforeExecution() {
        messenger().showLoading("Creating card");
    }
}
