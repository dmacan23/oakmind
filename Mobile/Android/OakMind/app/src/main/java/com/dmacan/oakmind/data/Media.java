package com.dmacan.oakmind.data;

import com.google.gson.annotations.Expose;

/**
 * Created by David on 8.9.2014..
 */
public class Media extends DataModel {

    @Expose
    private String location;
    @Expose
    private String label;
    @Expose
    private MediaType mediaType;

    public Media() {
        super();
    }

    public Media(String location, String label, MediaType mediaType) {
        super();
        this.location = location;
        this.label = label;
        this.mediaType = mediaType;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public MediaType getMediaType() {
        return mediaType;
    }

    public void setMediaType(MediaType mediaType) {
        this.mediaType = mediaType;
    }
}
