package com.dmacan.oakmind.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dmacan.oakmind.OakMindActivity;
import com.dmacan.oakmind.widget.MagicMessenger;

import butterknife.ButterKnife;

/**
 * Created by David on 16.8.2014..
 */
public abstract class OakMindFragment extends Fragment {

    private Menu menu;
    private Activity activity;

    public abstract int provideLayoutRes();

    public abstract void main();

    public OakMindActivity getOakMindActivity() {
        if (getActivity() != null)
            return (OakMindActivity) getActivity();
        return (OakMindActivity) activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.setHasOptionsMenu(true);
        View view = inflater.inflate(provideLayoutRes(), container, false);
        ButterKnife.inject(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        main();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        this.menu = menu;
    }

    public void setTitle(String title) {
        this.getOakMindActivity().getActionBar().setTitle(title);
    }

    public void setTitle(int resId) {
        this.getOakMindActivity().getActionBar().setTitle(resId);
    }

    public MagicMessenger messenger() {
        MagicMessenger messenger = this.getOakMindActivity().messenger();
        if (messenger == null)
            return new MagicMessenger(getOakMindActivity());
        return messenger;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = activity;
    }

    public Menu getMenu() {
        return menu;
    }
}
