package com.dmacan.oakmind.widget;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.dmacan.oakmind.R;

/**
 * Created by David on 15.9.2014..
 */
public class ConfirmDialog extends MagicDialog implements View.OnClickListener {

    Button btnDialogConfirm;
    Button btnDialogCancel;
    TextView txtDialogTitle;
    TextView txtDialogBody;
    private OnDialogConfirmListener onDialogConfirmListener;
    private String dialogMessage;
    private String dialogTitle;
    private String dialogConfirmText;
    private String dialogCancelText;

    public ConfirmDialog(Context context) {
        super(context, R.layout.dialog_confirm);
        init();
    }

    private void init() {
        txtDialogTitle = (TextView) findViewById(R.id.txtDialogTitle);
        txtDialogBody = (TextView) findViewById(R.id.txtDialogBody);
        btnDialogConfirm = (Button) findViewById(R.id.btnDialogConfirm);
        btnDialogCancel = (Button) findViewById(R.id.btnDialogCancel);
        btnDialogConfirm.setOnClickListener(this);
        btnDialogCancel.setOnClickListener(this);
    }


    public void setOnDialogConfirmListener(OnDialogConfirmListener onDialogConfirmListener) {
        this.onDialogConfirmListener = onDialogConfirmListener;
    }

    public void setDialogMessage(String dialogMessage) {
        this.dialogMessage = dialogMessage;
        txtDialogBody.setText(dialogMessage);
    }

    public void setDialogTitle(String dialogTitle) {
        this.dialogTitle = dialogTitle;
        txtDialogTitle.setText(dialogTitle);
    }

    public void setDialogConfirmText(String dialogConfirmText) {
        this.dialogConfirmText = dialogConfirmText;
        btnDialogConfirm.setText(dialogConfirmText);
    }

    public void setDialogCancelText(String dialogCancelText) {
        this.dialogCancelText = dialogCancelText;
        btnDialogCancel.setText(dialogCancelText);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnDialogCancel:
                if (onDialogConfirmListener != null)
                    onDialogConfirmListener.onDialogConfirm(false);
                break;
            case R.id.btnDialogConfirm:
                if (onDialogConfirmListener != null)
                    onDialogConfirmListener.onDialogConfirm(true);
                break;
        }
    }

    public interface OnDialogConfirmListener {
        public void onDialogConfirm(boolean positive);
    }
}
