package com.dmacan.oakmind;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;

import com.dmacan.oakmind.api.listener.OnDataDeletedListener;
import com.dmacan.oakmind.api.listener.OnDataUpdateListener;
import com.dmacan.oakmind.api.response.DeleteResponse;
import com.dmacan.oakmind.api.response.UpdateResponse;
import com.dmacan.oakmind.data.Deck;
import com.dmacan.oakmind.data.controller.DeckController;
import com.dmacan.oakmind.fragment.DeckFragment;
import com.dmacan.oakmind.fragment.ExamFragment;
import com.dmacan.oakmind.util.APIUtil;
import com.dmacan.oakmind.util.Const;
import com.dmacan.oakmind.widget.ConfirmDialog;
import com.dmacan.oakmind.widget.drawer.DrawerData;
import com.dmacan.oakmind.widget.drawer.DrawerItem;
import com.dmacan.oakmind.widget.drawer.DrawerItemHeading;
import com.dmacan.oakmind.widget.drawer.OakMindDrawerActivity;

import net.simonvt.menudrawer.MenuDrawer;
import net.simonvt.menudrawer.Position;

import java.util.ArrayList;
import java.util.List;

import hr.mtlab.dmacan.magicui.graphics.MagicDrawable;

/**
 * Created by David on 24.8.2014..
 */
public class DeckActivity extends OakMindDrawerActivity implements OnDataDeletedListener, OnDataUpdateListener {

    private Deck deck;
    private ConfirmDialog.OnDialogConfirmListener deleteConfirmListener = new ConfirmDialog.OnDialogConfirmListener() {
        @Override
        public void onDialogConfirm(boolean positive) {
            if (positive) {
                deckController.setOnDataDeletedListener(DeckActivity.this);
                deckController.deleteDeck(deck);
            }
            confirmDialog.dismiss();
        }
    };
    private ConfirmDialog.OnDialogConfirmListener publishConfirmListener = new ConfirmDialog.OnDialogConfirmListener() {
        @Override
        public void onDialogConfirm(boolean positive) {
            if (positive) {
                deckController.setOnDataUpdateListener(DeckActivity.this);
                deckController.publishDeck(deck);
            }
            confirmDialog.dismiss();
        }
    };
    private DeckController deckController;
    private DeckFragment defaultFragment;
    private ConfirmDialog confirmDialog;

    @Override
    protected int getDragMode() {
        return MenuDrawer.MENU_DRAG_WINDOW;
    }

    @Override
    protected Position getDrawerPosition() {
        return Position.LEFT;
    }

    @Override
    public void main(Bundle savedInstanceState, boolean drawerActivity) {
        deckController = new DeckController();
        confirmDialog = new ConfirmDialog(this);
    }

    @Override
    public List<DrawerItem> provideDrawerItems() {
        deck = APIUtil.createGson().fromJson(getIntent().getExtras().getString(Const.Key.DECK_CURRENT), Deck.class);
        boolean isDeckOwned = getIntent().getExtras().getBoolean(Const.Key.DECK_OWNED);
        List<DrawerItem> drawerItems = new ArrayList<DrawerItem>();
        DrawerData.context = getBaseContext();
        String symbolString = Html.fromHtml(deck.getCategory().getSymbol()).toString();
        // TODO add proper size in dp
        MagicDrawable symbol = new MagicDrawable(getBaseContext(), symbolString).sizeDp(100).colorRes(R.color.white);
        drawerItems.add(new DrawerItemHeading(deck.getLabel(), Color.parseColor(deck.getColor()), symbol));
        if (isDeckOwned) {
//            drawerItems.add(DrawerData.newItem(R.string.fi_settings, R.string.di_edit));
            drawerItems.add(DrawerData.newItem(R.string.fi_delete, R.string.di_delete, false));
            drawerItems.add(DrawerData.newItem(R.string.fi_add, R.string.di_add_card, false));
            drawerItems.add(DrawerData.newItem(R.string.fi_share, R.string.di_share, false));
        }
        drawerItems.add(DrawerData.newItem(R.string.fi_lightbulb, R.string.di_exam, true));
        return drawerItems;
    }

    // TODO dodati R.string resurse
    @Override
    public Fragment provideFragment(String tag) {
        if (defaultFragment == null)
            defaultFragment = new DeckFragment();
        if (tag.equals(DrawerData.DEFAULT_TAG) || tag.equals(deck.getLabel()))
            return defaultFragment;
        else if (tag.equals(getOakMindString(R.string.di_edit)))
            startDeckEditActivity();
        else if (tag.equals("Delete")) {
            confirmDialog.setDialogTitle("Delete deck");
            confirmDialog.setDialogMessage("Are you sure you want to do this?");
            confirmDialog.setOnDialogConfirmListener(deleteConfirmListener);
            confirmDialog.show();
        } else if (tag.equals("Exam")) {
            if (this.deck.getCards() != null && this.deck.getCards().size() > 0)
                return new ExamFragment();
            else
                messenger().showError("You need cards for the exam");
        } else if (tag.equals("Add card")) {
            startCardCreateActivity();
        } else if (tag.equals(getOakMindString(R.string.di_share))) {
            if (this.deck.getCards() != null && this.deck.getCards().size() > 4) {
                confirmDialog.setDialogTitle("Publish deck");
                confirmDialog.setDialogMessage("Are you sure you want to do this?");
                confirmDialog.setOnDialogConfirmListener(publishConfirmListener);
                confirmDialog.show();
            } else
                messenger().showError("You need to have at least 5 cards in order to publish your deck");
        }
        return null;
    }

    public void showDefaultFragment() {
        if (defaultFragment == null)
            defaultFragment = new DeckFragment();
        attachFragment(defaultFragment, deck.getLabel());
        commitTransactions();
    }

    public void startCardCreateActivity() {
        Intent i = new Intent(this, CardCreateActivity.class);
        i.putExtra(Const.Key.DECK_CURRENT, APIUtil.createGson().toJson(deck));
        this.startActivity(i);
    }

    public void startDeckEditActivity() {
        Intent i = new Intent(this, DeckCreateActivity.class);
        i.putExtra(Const.Key.DECK_CURRENT, APIUtil.createGson().toJson(deck));
        this.startActivity(i);
    }

    public Deck getDeck() {
        return deck;
    }

    @Override
    public int provideLayoutRes() {
        return R.layout.activity_main;
    }

    @Override
    public void onDataDeleted(DeleteResponse response) {
        messenger().showSuccess("Deck deleted");
        this.finish();
    }

    @Override
    public void beforeExecution() {
        messenger().showLoading("Deleting deck");
    }

    @Override
    public void onDataUpdate(UpdateResponse response) {
        messenger().showSuccess("Deck is now visible for everyone");
    }
}
