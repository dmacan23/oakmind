package com.dmacan.oakmind;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.WindowManager;

import com.dmacan.oakmind.widget.MagicMessenger;
import com.dmacan.oakmind.widget.Messenger;
import com.github.johnpersano.supertoasts.SuperToast;

import de.keyboardsurfer.android.widget.crouton.Crouton;

/**
 * Created by David on 16.8.2014..
 */
public abstract class OakMindActivity extends ActionBarActivity {

    private Bundle savedInstanceState;

    private Messenger loadingMessenger;
    private Messenger standardMessenger;
    private Messenger progressMessenger;
    private Messenger errorMessenger;
    private Messenger successMessenger;
    private int layoutRes;
    private Fragment currentFragment;
    private int container;
    private MagicMessenger messenger;


    /**
     * A method which should provide resource ID for the wanted layout
     *
     * @return resource ID
     */
    public abstract int provideLayoutRes();

    /**
     * A method which is called immediately after the activity has been created
     */
    public abstract void main(Bundle savedInstanceState);

    /**
     * Sets up the desired fragment inside the activity
     *
     * @param container
     * @param fragment
     */
    public void setupFragment(int container, Fragment fragment) {
        if (savedInstanceState == null) {
            this.currentFragment = fragment;
            this.container = container;
            getSupportFragmentManager().beginTransaction()
                    .replace(container, fragment)
                    .commit();
        }
    }

    private void init(Bundle savedInstanceState) {
        layoutRes = provideLayoutRes();
        setContentView(layoutRes);
        this.savedInstanceState = savedInstanceState;
        this.messenger = new MagicMessenger(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init(savedInstanceState);
        main(savedInstanceState);
    }

    public void disableUserInteraction() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    public void enableUserInteraction() {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    public Messenger getMessenger() {
        this.standardMessenger = new Messenger(this, SuperToast.Type.STANDARD);
        return this.standardMessenger;
    }

    public Messenger getLoadingMessenger() {
        if (this.loadingMessenger == null || !this.loadingMessenger.isShowing()) {
            this.loadingMessenger = new Messenger(this, SuperToast.Type.PROGRESS);
            this.loadingMessenger.setDuration(0);
            this.loadingMessenger.setIndeterminate(true);
            this.loadingMessenger.setProgressIndeterminate(true);
        }
        return this.loadingMessenger;
    }

    public Messenger getProgressMessenger(int max) {
        this.progressMessenger = new Messenger(this, SuperToast.Type.PROGRESS_HORIZONTAL);
        return this.progressMessenger;
    }

    public Messenger getErrorMessenger() {
        this.errorMessenger = new Messenger(this, SuperToast.Type.STANDARD);
        this.errorMessenger.setBackground(SuperToast.Background.RED);
        return this.errorMessenger;
    }

    public Messenger getSuccessMessenger() {
        this.successMessenger = new Messenger(this, SuperToast.Type.STANDARD);
        this.successMessenger.setBackground(SuperToast.Background.GREEN);
        return this.successMessenger;
    }

    public void sleep(long seconds) {
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public int getBtnId(int btn) {
        switch (btn) {
            case 1:
                return com.gitonway.lee.niftymodaldialogeffects.lib.R.id.button1;
            case 2:
                return com.gitonway.lee.niftymodaldialogeffects.lib.R.id.button2;
        }
        return 0;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Crouton.cancelAllCroutons();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
        } else {
        }
    }

    public MagicMessenger messenger() {
        if (this.messenger == null)
            this.messenger = new MagicMessenger(this);
        return this.messenger;
    }
}
