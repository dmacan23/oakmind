package com.dmacan.oakmind.api.listener;

import com.dmacan.oakmind.data.DataModel;

/**
 * Created by David on 8.9.2014..
 */
public interface OnDataMultipleReadListener extends OnDataCRUDListener {

    public void onDataMultipleRead(DataModel[] result);

}
