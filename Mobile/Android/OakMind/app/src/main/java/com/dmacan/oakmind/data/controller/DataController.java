package com.dmacan.oakmind.data.controller;

import android.content.Context;

import com.dmacan.oakmind.api.OakMindAPIv2;
import com.dmacan.oakmind.api.listener.OnDataCreatedListener;
import com.dmacan.oakmind.api.listener.OnDataDeletedListener;
import com.dmacan.oakmind.api.listener.OnDataMultipleReadListener;
import com.dmacan.oakmind.api.listener.OnDataReadListener;
import com.dmacan.oakmind.api.listener.OnDataUpdateListener;
import com.dmacan.oakmind.api.listener.OnFileUploadListener;
import com.dmacan.oakmind.api.listener.OnNetworkErrorListener;
import com.dmacan.oakmind.api.request.CreateRequestNew;
import com.dmacan.oakmind.api.request.ReadRequestNew;
import com.dmacan.oakmind.api.response.CreateResponse;
import com.dmacan.oakmind.api.response.DeleteResponse;
import com.dmacan.oakmind.api.response.FileUploadResponse;
import com.dmacan.oakmind.api.response.UpdateResponse;
import com.dmacan.oakmind.util.APIUtil;
import com.dmacan.oakmind.util.Util;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by David on 8.9.2014..
 */
public abstract class DataController {

    private OakMindAPIv2 oakMindAPI;
    private OnNetworkErrorListener onNetworkErrorListener;
    private OnDataReadListener onDataReadListener;
    private OnDataMultipleReadListener onDataMultipleReadListener;
    private OnDataCreatedListener onDataCreatedListener;
    private Callback<CreateResponse> createCallback = new Callback<CreateResponse>() {
        @Override
        public void success(CreateResponse response, Response response2) {
            if (onDataCreatedListener != null)
                onDataCreatedListener.onDataCreated(response);
        }

        @Override
        public void failure(RetrofitError error) {
            Crouton.cancelAllCroutons();
            if (onNetworkErrorListener != null)
                onNetworkErrorListener.onNetworkError(error);
        }
    };
    private OnDataDeletedListener onDataDeletedListener;
    private Callback<DeleteResponse> deleteCallback = new Callback<DeleteResponse>() {
        @Override
        public void success(DeleteResponse deleteResponse, Response response) {
            if (onDataDeletedListener != null)
                onDataDeletedListener.onDataDeleted(deleteResponse);
        }

        @Override
        public void failure(RetrofitError error) {
            Crouton.cancelAllCroutons();
            if (onNetworkErrorListener != null)
                onNetworkErrorListener.onNetworkError(error);
        }
    };
    private OnDataUpdateListener onDataUpdateListener;
    private Callback<UpdateResponse> updateCallback = new Callback<UpdateResponse>() {
        @Override
        public void success(UpdateResponse updateResponse, Response response) {
            if (onDataUpdateListener != null)
                onDataUpdateListener.onDataUpdate(updateResponse);
        }

        @Override
        public void failure(RetrofitError error) {
            Crouton.cancelAllCroutons();
            if (onNetworkErrorListener != null)
                onNetworkErrorListener.onNetworkError(error);
        }
    };


    private OnFileUploadListener onFileUploadListener;
    private Callback<FileUploadResponse> fileUploadCallback = new Callback<FileUploadResponse>() {
        @Override
        public void success(FileUploadResponse fileUploadResponse, Response response) {
            if (onFileUploadListener != null)
                onFileUploadListener.onFileUploaded(fileUploadResponse);
        }

        @Override
        public void failure(RetrofitError error) {
            Crouton.cancelAllCroutons();
            if (onNetworkErrorListener != null)
                onNetworkErrorListener.onNetworkError(error);
        }
    };
    private String readMethod;

    public DataController() {
        this.oakMindAPI = APIUtil.getRestAdapter().create(OakMindAPIv2.class);
    }

    public OakMindAPIv2 getOakMindAPI() {
        return oakMindAPI;
    }

    public void setOakMindAPI(OakMindAPIv2 oakMindAPI) {
        this.oakMindAPI = oakMindAPI;
    }

    public void createItem(CreateRequestNew request, Callback<CreateResponse> callback) {
        this.oakMindAPI.createItem(request, callback);
    }

    public void showNetworkError(RetrofitError response) {
        if (onNetworkErrorListener != null)
            onNetworkErrorListener.onNetworkError(response);
    }

    public boolean isNetworkConnectionAvailable(Context context) {
        return (Util.isNetworkConnectionAvailable(context));
    }

    public ReadRequestNew constructReadRequest(String method) {
        return new ReadRequestNew();
    }

    public String getReadMethod() {
        return readMethod;
    }

    public void setReadMethod(String readMethod) {
        this.readMethod = readMethod;
    }

    public OnNetworkErrorListener getOnNetworkErrorListener() {
        return onNetworkErrorListener;
    }

    public void setOnNetworkErrorListener(OnNetworkErrorListener onNetworkErrorListener) {
        this.onNetworkErrorListener = onNetworkErrorListener;
    }

    public OnDataReadListener getOnDataReadListener() {
        return onDataReadListener;
    }

    public void setOnDataReadListener(OnDataReadListener onDataReadListener) {
        this.onDataReadListener = onDataReadListener;
    }

    public OnDataMultipleReadListener getOnDataMultipleReadListener() {
        return onDataMultipleReadListener;
    }

    public void setOnDataMultipleReadListener(OnDataMultipleReadListener onDataMultipleReadListener) {
        this.onDataMultipleReadListener = onDataMultipleReadListener;
    }

    public OnDataCreatedListener getOnDataCreatedListener() {
        return onDataCreatedListener;
    }

    public void setOnDataCreatedListener(OnDataCreatedListener onDataCreatedListener) {
        this.onDataCreatedListener = onDataCreatedListener;
    }

    public Callback<CreateResponse> getCreateCallback() {
        return createCallback;
    }

    public OnFileUploadListener getOnFileUploadListener() {
        return onFileUploadListener;
    }

    public void setOnFileUploadListener(OnFileUploadListener onFileUploadListener) {
        this.onFileUploadListener = onFileUploadListener;
    }

    public Callback<FileUploadResponse> getFileUploadCallback() {
        return fileUploadCallback;
    }

    public OnDataDeletedListener getOnDataDeletedListener() {
        return onDataDeletedListener;
    }

    public void setOnDataDeletedListener(OnDataDeletedListener onDataDeletedListener) {
        this.onDataDeletedListener = onDataDeletedListener;
    }

    public OnDataUpdateListener getOnDataUpdateListener() {
        return onDataUpdateListener;
    }

    public void setOnDataUpdateListener(OnDataUpdateListener onDataUpdateListener) {
        this.onDataUpdateListener = onDataUpdateListener;
    }

    public Callback<DeleteResponse> getDeleteCallback() {
        return deleteCallback;
    }

    public Callback<UpdateResponse> getUpdateCallback() {
        return updateCallback;
    }
}
