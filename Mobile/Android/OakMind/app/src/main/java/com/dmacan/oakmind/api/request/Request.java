package com.dmacan.oakmind.api.request;

import com.google.gson.annotations.Expose;

/**
 * Created by David on 8.9.2014..
 */
public class Request {

    public static final String METHOD_FULL = "full";

    @Expose
    private String method;

    public Request() {
        this.method = METHOD_FULL;
    }

    public Request(String method) {
        this.method = method;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }
}
