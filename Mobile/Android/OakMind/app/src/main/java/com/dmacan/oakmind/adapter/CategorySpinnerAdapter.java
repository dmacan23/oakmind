package com.dmacan.oakmind.adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.dmacan.oakmind.R;
import com.dmacan.oakmind.data.DeckCategory;

import java.util.ArrayList;
import java.util.List;

import hr.mtlab.dmacan.magicui.widget.MagicTextView;
import hr.mtlab.dmacan.magicui.widget.util.MagicFont;

/**
 * Created by David on 23.8.2014..
 */
public class CategorySpinnerAdapter extends BaseAdapter {

    private Context context;
    private List<DeckCategory> items;

    public CategorySpinnerAdapter(Context context) {
        this.context = context;
        this.items = new ArrayList<DeckCategory>();
    }

    public CategorySpinnerAdapter(Context context, List<DeckCategory> items) {
        this.context = context;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public DeckCategory getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.item_spinner_simple, parent, false);
        displayData(convertView, items.get(position));
        return convertView;
    }

    public boolean addItem(DeckCategory category) {
        boolean success = this.items.add(category);
        this.notifyDataSetChanged();
        this.notifyDataSetInvalidated();
        return success;
    }

    // TODO enable to set icon directly, instead of concatenating strings into label
    private void displayData(View v, DeckCategory c) {
        TextView txtIcon = ((TextView) v.findViewById(R.id.spinnerSimpleIcon));
        txtIcon.setText(Html.fromHtml(c.getSymbol()));
        MagicFont.setFont("fontawesome.ttf", txtIcon);
        ((MagicTextView) v.findViewById(R.id.spinnerSimpleLabel)).setText(c.getLabel());
    }
}
