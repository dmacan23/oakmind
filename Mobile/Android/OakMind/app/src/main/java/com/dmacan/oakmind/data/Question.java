package com.dmacan.oakmind.data;

import com.google.gson.annotations.Expose;

/**
 * Created by David on 8.9.2014..
 */
public class Question extends DataModel {

    @Expose
    private String title;
    @Expose
    private MediaType mediaType;

    public Question(String title) {
        super();
        this.title = title;
    }

    public Question() {
        super();
    }

    public Question(String title, MediaType mediaType) {
        super();
        this.title = title;
        this.mediaType = mediaType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public MediaType getMediaType() {
        return mediaType;
    }

    public void setMediaType(MediaType mediaType) {
        this.mediaType = mediaType;
    }
}
