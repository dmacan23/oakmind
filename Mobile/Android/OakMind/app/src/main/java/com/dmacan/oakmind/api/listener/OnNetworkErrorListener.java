package com.dmacan.oakmind.api.listener;

import retrofit.RetrofitError;

/**
 * Created by David on 8.9.2014..
 */
public interface OnNetworkErrorListener {

    public void onNetworkError(RetrofitError error);

}
