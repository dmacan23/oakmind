package hr.mtlab.dmacan.magicui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.EditText;

import hr.mtlab.dmacan.magicui.graphics.MagicDrawable;
import hr.mtlab.dmacan.magicui.util.Const;
import hr.mtlab.dmacan.magicui.widget.util.MagicFont;
import hr.mtlab.dmacan.magicui.widget.util.MagicIcon;
import hr.mtlab.magic.ui.R;


/**
 * Created by David on 19.8.2014..
 */
public class MagicEditText extends EditText implements Magic {

    private String font;
    private String iconFont;
    private String fontPath;
    private String iconFontPath;
    private String iconCode;
    private Drawable iconDrawable;
    private MagicUI.Position iconPosition;
    private int iconColor;
    private float iconPadding;
    private float iconDp;

    public MagicEditText(Context context) {
        super(context);
        init(null, R.attr.MagicEditTextStyle);
    }

    public MagicEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, R.attr.MagicEditTextStyle);
    }

    public MagicEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs, defStyle);
    }

    private void init(AttributeSet attrs, int defStyle) {
        if (isInEditMode()) return;
        TypedArray attributes = getContext().obtainStyledAttributes(attrs, R.styleable.Magic, defStyle, 0);
        this.iconDrawable = attributes.getDrawable(R.styleable.Magic_iconDrawable);
        this.iconCode = attributes.getString(R.styleable.Magic_iconCode);
        this.iconPosition = MagicUI.Position.fromId(attributes.getInt(R.styleable.Magic_iconPosition, 0));
        this.iconPadding = attributes.getDimension(R.styleable.Magic_iconPadding, 0);
        this.font = attributes.getString(R.styleable.Magic_font);
        this.iconFont = attributes.getString(R.styleable.Magic_iconFont);
        this.fontPath = attributes.getString(R.styleable.Magic_fontLocation);
        this.iconFontPath = attributes.getString(R.styleable.Magic_iconFontLocation);
        this.iconColor = attributes.getColor(R.styleable.Magic_iconColor, android.R.color.black);
        this.iconDp = attributes.getDimension(R.styleable.Magic_iconDp, getTextSize());
        if (iconFontPath == null && fontPath == null)
            iconFontPath = fontPath = Const.Default.FONT_LOCATION;
        if (iconFont == null && font == null)
            iconFont = Const.Default.FONT;
        attributes.recycle();
        initFont();
        initIcon();
    }

    private void initFont() {
        if (font != null) {
            addFont(font);
        }
    }

    private void initIcon() {
        Drawable drawable = null;
        if (iconDrawable != null) {
            drawable = iconDrawable;
            iconCode = null;
        }
        if (iconCode != null) {
            if (iconFont == null)
                iconFont = font;
            if (iconFontPath == null)
                iconFontPath = fontPath;
            drawable = new MagicDrawable(getContext(), iconCode, iconFont, iconFontPath);
            ((MagicDrawable) drawable).sizeDp((int) this.iconDp);
            ((MagicDrawable) drawable).color(iconColor);
            addIcon(drawable);
        }
        addIcon(drawable);
        addIconPadding(iconPadding);
    }

    @Override
    public void addIcon(Drawable icon) {
        MagicIcon.addIcon(icon, this);
    }

    @Override
    public void addIcon(Drawable icon, MagicUI.Position position) {
        MagicIcon.addIcon(icon, position, this);
    }

    @Override
    public void addIcon(String iconCode) {
        MagicIcon.addIcon(new MagicDrawable(getContext(), iconCode), this);
    }

    @Override
    public void addIcon(String iconCode, MagicUI.Position position) {
        MagicIcon.addIcon(new MagicDrawable(getContext(), iconCode), position, this);
    }

    @Override
    public void addIcon(String iconCode, String iconFontPath) {
        MagicIcon.addIcon(new MagicDrawable(getContext(), iconCode, iconFont, iconFontPath), this);
    }

    @Override
    public void addIcon(String iconCode, MagicUI.Position position, String iconFontPath) {
        MagicIcon.addIcon(new MagicDrawable(getContext(), iconCode, iconFont, iconFontPath), position, this);
    }

    @Override
    public void setIconFontColor(int color) {
        if (iconCode != null) {
            MagicDrawable iconDrawable = new MagicDrawable(getContext(), iconCode);
            iconDrawable.colorRes(color);
        }
    }

    @Override
    public void addIconPadding(float dp) {
        MagicIcon.addIconPadding(dp, this);
    }

    @Override
    public void addFont(String font) {
        MagicFont.setFont(font, this);
    }

    @Override
    public void addFont(String font, String location) {
        MagicFont.setFont(font, location, this);
    }
}
