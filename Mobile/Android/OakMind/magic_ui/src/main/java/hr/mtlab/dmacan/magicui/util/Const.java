package hr.mtlab.dmacan.magicui.util;

/**
 * Created by David on 19.8.2014..
 */
public class Const {

    public static class Default {
        public static final String FONT = "fontawesome.ttf";
        public static final String FONT_LOCATION = "fonts/";
        public static final int ANDROID_ACTIONBAR_ICON_SIZE_DP = 24;
    }

}
