package hr.mtlab.dmacan.magicui.widget.colorpicker;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;

import hr.mtlab.magic.ui.R;

/**
 * Created by David on 5.9.2014..
 */
public class ColorItem extends View {

    private int color = Color.TRANSPARENT;

    private Drawable circleDrawable;

    public ColorItem(Context context) {
        super(context);
    }

    public ColorItem(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0);
    }

    public ColorItem(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs, defStyleAttr);
    }

    private void init(AttributeSet attrs, int defStyle) {
        if (attrs != null) {
            TypedArray attributes = getContext().obtainStyledAttributes(attrs, R.styleable.MagicColorItem, defStyle, 0);
            this.color = attributes.getColor(R.styleable.MagicColorItem_color, color);
        }
        setupDrawable();
        setupBackground(circleDrawable);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void setupBackground(Drawable drawable) {
        int sdk = android.os.Build.VERSION.SDK_INT;
        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            //setBackgroundDrawable(drawable);
        } else {
            //setBackground(drawable);
        }
    }

    private void setupDrawable() {
        this.circleDrawable = getContext().getResources().getDrawable(R.drawable.shape_circle);
        this.circleDrawable.setBounds(0, 0, this.getMeasuredHeight(), this.getMeasuredWidth());
        this.circleDrawable.setColorFilter(color, PorterDuff.Mode.SRC_OVER);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int x = canvas.getWidth() / 2;
        int y = canvas.getHeight() / 2;
        int r = (int) (getSmallerSize(canvas) - (getSmallerSize(canvas) * 0.05)) / 2;
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setColor(color);
        canvas.drawCircle(x, y, r, paint);
    }

    private int getSmallerSize(Canvas canvas) {
        return (this.getMeasuredHeight() > this.getMeasuredWidth()) ? this.getMeasuredWidth() : this.getMeasuredHeight();
    }

    public int getColor() {
        return this.color;
    }

    public void setColor(int color) {
        this.color = color;
        init(null, 0);
    }

}
