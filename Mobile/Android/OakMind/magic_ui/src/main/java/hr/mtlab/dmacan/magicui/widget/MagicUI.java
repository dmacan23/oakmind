package hr.mtlab.dmacan.magicui.widget;

/**
 * Created by David on 19.8.2014..
 */
public class MagicUI {

    public enum Position {
        LEFT(0),
        TOP(1),
        RIGHT(2),
        BOTTOM(3),
        HORIZONTAL(4),
        VERTICAL(5),
        ALL(6);

        int id;

        Position(int id) {
            this.id = id;
        }

        static Position fromId(int id) {
            for (Position position : values()) {
                if (position.id == id) return position;
            }
            throw new IllegalArgumentException();
        }
    }

}
