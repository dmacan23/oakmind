package hr.mtlab.dmacan.magicui.widget.colorpicker;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.List;

import hr.mtlab.magic.ui.R;

/**
 * Created by David on 5.9.2014..
 */
public class MagicColorPickerAdapter extends BaseAdapter {

    private List<Integer> colors;
    private Context context;

    public MagicColorPickerAdapter(Context context) {
        this.context = context;
    }

    public MagicColorPickerAdapter(List<Integer> colors, Context context) {
        this.colors = colors;
        this.context = context;
    }

    @Override
    public int getCount() {
        return colors.size();
    }

    @Override
    public Integer getItem(int position) {
        return colors.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void addItem(Integer color) {
        this.colors.add(color);
        this.notifyDataSetChanged();
        this.notifyDataSetInvalidated();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ColorItem colorItem = (ColorItem) inflater.inflate(R.layout.item_color, parent, false);
        colorItem.setColor(colors.get(position));
        return colorItem;
    }

}
