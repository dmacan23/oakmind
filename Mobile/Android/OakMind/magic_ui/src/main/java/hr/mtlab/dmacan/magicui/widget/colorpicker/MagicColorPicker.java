package hr.mtlab.dmacan.magicui.widget.colorpicker;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.GridView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by David on 5.9.2014..
 */
public class MagicColorPicker extends GridView {

    private List<Integer> colors;
    private MagicColorPickerAdapter adapter;

    public MagicColorPicker(Context context, List<Integer> colors) {
        super(context);
        this.colors = colors;
        init();
    }

    public MagicColorPicker(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MagicColorPicker(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public void setColors(List<Integer> colors) {
        this.colors = colors;
        for (Integer color : colors)
            adapter.addItem(color);
    }

    private void init() {
        if (this.colors == null)
            this.colors = new ArrayList<Integer>();
        adapter = new MagicColorPickerAdapter(colors, getContext());
        this.setAdapter(adapter);
    }

    public void addColor(Integer color) {
        adapter.addItem(color);
    }

}
