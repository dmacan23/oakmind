package hr.mtlab.dmacan.magicui.widget.util;

import android.graphics.drawable.Drawable;
import android.widget.TextView;

import hr.mtlab.dmacan.magicui.widget.MagicUI;


/**
 * Created by David on 19.8.2014..
 */
public class MagicIcon {

    public static boolean addIcon(Drawable icon, MagicUI.Position position, TextView... textViews) {
        if (textViews.length > 0) {
            for (TextView textView : textViews) {
                switch (position) {
                    case TOP:
                        textView.setCompoundDrawablesWithIntrinsicBounds(null, icon, null, null);
                        break;
                    case BOTTOM:
                        textView.setCompoundDrawablesWithIntrinsicBounds(null, null, null, icon);
                        break;
                    case RIGHT:
                        textView.setCompoundDrawablesWithIntrinsicBounds(null, null, icon, null);
                        break;
                    case HORIZONTAL:
                        textView.setCompoundDrawablesWithIntrinsicBounds(icon, null, icon, null);
                        break;
                    case VERTICAL:
                        textView.setCompoundDrawablesWithIntrinsicBounds(null, icon, null, icon);
                        break;
                    case ALL:
                        textView.setCompoundDrawablesWithIntrinsicBounds(icon, icon, icon, icon);
                        break;
                    case LEFT:
                        textView.setCompoundDrawables(icon, null, null, null);
                        break;
                    default:
                        addIcon(icon);
                }
                return true;
            }
        }
        return false;
    }

    public static boolean addIcon(Drawable icon, TextView... textViews) {
        return addIcon(icon, MagicUI.Position.LEFT, textViews);
    }

    public static boolean addIconPadding(float padding, TextView... textViews) {
        for (TextView textView : textViews)
            textView.setCompoundDrawablePadding((int) padding);
        return textViews.length > 0;
    }


}

