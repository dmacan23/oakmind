package hr.mtlab.dmacan.magicui.widget;

import android.graphics.drawable.Drawable;

/**
 * Created by David on 19.8.2014..
 */
public interface Magic {

    /**
     * Adds drawable as icon to the left side of the view
     *
     * @param icon Drawable icon
     */
    public void addIcon(Drawable icon);

    /**
     * Adds drawable as icon to the specified side of the view
     *
     * @param icon     Drawable icon
     * @param position Icon placement position
     */
    public void addIcon(Drawable icon, MagicUI.Position position);

    /**
     * Adds iconic font character as view's icon to the left side of that view
     *
     * @param iconCode Unicode value of the icon
     */
    public void addIcon(String iconCode);

    /**
     * Adds iconic font character as view's icon to the desired side of that view
     *
     * @param iconCode Unicode value of the icon
     * @param position Icon placement position
     */
    public void addIcon(String iconCode, MagicUI.Position position);

    /**
     * Adds specified iconic font character as view's icon to the left side of that view
     *
     * @param iconCode     Unicode value of the icon
     * @param iconFontPath Path to the iconic font
     */
    public void addIcon(String iconCode, String iconFontPath);

    /**
     * Adds specified iconic font character as view's icon to the desired side of that view
     *
     * @param iconCode     Unicode value of the icon
     * @param position     Icon placement position
     * @param iconFontPath Path to the iconic font
     */
    public void addIcon(String iconCode, MagicUI.Position position, String iconFontPath);

    /**
     * Sets the color of the iconic font icon
     *
     * @param colorRes Color resource for the icon
     */
    public void setIconFontColor(int colorRes);

    /**
     * Adds padding to the icon in the desired position
     *
     * @param dp Amount of padding in dp
     */
    public void addIconPadding(float dp);

    /**
     * Changes view's default font from the fonts subdirectory within the Assets resource directory
     *
     * @param font font to be added to the view
     */
    public void addFont(String font);

    /**
     * Changes view's default font
     *
     * @param font     font to be added to the view
     * @param location location of the font in Assets resource directory
     */
    public void addFont(String font, String location);
}
