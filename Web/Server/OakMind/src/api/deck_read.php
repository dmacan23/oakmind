<?php
/**
 * Created by PhpStorm.
 * User: David
 * Date: 30.08.14.
 * Time: 15:49
 */


require_once '../database/DatabaseAdapter.php';

$_POST = json_decode(file_get_contents('php://input'), true);

if (!empty($_POST)) {
    DatabaseAdapter::log("Usel sam v POST");
    DatabaseAdapter::log("POST: " . $_POST['user']);
    $res = DatabaseAdapter::readDecksFromUser($_POST['user']);
    DatabaseAdapter::log($res);
    echo $res;
}
