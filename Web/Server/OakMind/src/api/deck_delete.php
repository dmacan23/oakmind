<?php
/**
 * Created by PhpStorm.
 * User: David
 * Date: 30.08.14.
 * Time: 22:24
 */

require_once '../database/DatabaseAdapter.php';

$_POST = json_decode(file_get_contents('php://input'), true);

if (!empty($_POST)) {
    DatabaseAdapter::deleteDeck($_POST['id']);
}
