<?php
/**
 * Created by PhpStorm.
 * User: David
 * Date: 16.08.14.
 * Time: 16:43
 */
require_once '../database/DatabaseAdapter.php';


$_POST = json_decode(file_get_contents('php://input'), true);
if (!empty($_POST)) {
    if (register())
        displayStatus(true);
    else
        displayStatus(false);
} else
    displayStatus(false);

function register()
{
    return DatabaseAdapter::createUser($_POST["username"], $_POST["password"], $_POST["email"], $_POST["avatar"]);
}

function displayStatus($success)
{
    if ($success)
        $message = 'OK';
    else
        $message = 'Error';
    $status = array('message' => $message);
    header('Content-type: text/javascript');
    echo json_encode($status);
}