<?php
/**
 * Created by PhpStorm.
 * User: David
 * Date: 30.08.14.
 * Time: 18:29
 */

require_once '../database/DatabaseAdapter.php';

$_POST = json_decode(file_get_contents('php://input'), true);

if (!empty($_POST)) {
    $res = DatabaseAdapter::readCardsFromDeck($_POST['deck']);
    DatabaseAdapter::log($res);
    echo $res;
}