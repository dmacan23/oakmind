<?php
/**
 * Created by PhpStorm.
 * User: David
 * Date: 08.09.14.
 * Time: 19:29
 */

require_once '../database/DBAdapterV2.php';

$_POST = json_decode(file_get_contents('php://input'), true);

if (!empty($_POST)) {
    read();
}

//$userRequest = array(
//    'username' => 'dmacan',
//    'password' => 'xxx123'
//);
//
//$deckRequest = array(
//    'id' => 65
//);
//
//$cardsRequest = array(
//    'id' => 26
//);
//
//$examsForDeckRequest = array(
//    'id' => 42
//);
//
//$examsForUserRequest = array(
//    'id' => 47
//);


//$db = new DBAdapterV2();

//echo 'READ REQUESTS<br>---------<br>';
//echo '<br><br><br>User login<br><br>';
//echo $db->readUserLogin($userRequest);
//echo '<br><br>Decks for user<br><br>';
//echo $db->readDecks($deckRequest);
//echo '<br><br>Decks public<br><br>';
//echo $db->readDecks();
//echo '<br><br>Cards for deck<br><br>';
//echo $db->readCards($cardsRequest);
//echo '<br><br>Exams for deck<br><br>';
//echo $db->readExamsForDeck($examsForDeckRequest);
//echo '<br><br>Exams for user<br><br>';
//echo $db->readExamsForUser($examsForUserRequest);
function read()
{
    $db = new DBAdapterV2();
    $params = $_POST['params'];
    switch ($_POST['method']) {
        case 'userLogin':
            echo $db->readUserLogin($params);
            break;
        case 'decksOwned':
            echo $db->readDecks($params);
            break;
        case 'decksPublic':
            echo $db->readDecks();
            break;
        case 'cardsForDeck':
            echo $db->readCards($params);
            break;
        case 'examsForDeck':
            echo $db->readExamsForDeck($params);
            break;
        case 'examsForUser':
            echo $db->readExamsForUser($params);
            break;
        case 'deckCategories':
            echo $db->readDeckCategories();
            break;
    }
}