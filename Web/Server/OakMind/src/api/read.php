<?php
/**
 * Created by PhpStorm.
 * User: David
 * Date: 07.09.14.
 * Time: 11:54
 */

require_once '../database/DatabaseAdapter.php';
require_once '../database/DBAdapter.php';

$_POST = json_decode(file_get_contents('php://input'), true);

if (!empty($_POST)) {
    read();
}

function read()
{
    $table = $_POST['table'];
    $distinct = $_POST['distinct'];
    $orderBy = $_POST['order_by'];
    $where = constructWhere($_POST['where']);
    $db = new DBAdapter();
    $result = $db->read($table, $where, $distinct, $orderBy);
    echo json_encode($result);
}

function constructWhere($whereArr)
{
    $where = '';
    foreach ($whereArr as $row) {
        if (!$row['string'])
            $value = $row['value'];
        else
            $value = "\"" . $row['value'] . "\"";
        $where .= $row['param'] . ' = ' . $value . ' AND ';
    }

    $where = substr($where, 0, -5);
    return $where;
}
