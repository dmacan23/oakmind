<?php
/**
 * Created by PhpStorm.
 * User: David
 * Date: 27.08.14.
 * Time: 23:39
 */


require_once '../database/DatabaseAdapter.php';


$_POST = json_decode(file_get_contents('php://input'), true);
if (!empty($_POST)) {
    returnDeck(DatabaseAdapter::createDeck($_POST['deck']));
}


function returnDeck($id)
{
    $arr = array(
        'remote_id' => $id,
        'message' => 'OK'
    );
    DatabaseAdapter::log(json_encode($arr));
    echo json_encode($arr);
}
