<?php
/**
 * Created by PhpStorm.
 * User: David
 * Date: 08.09.14.
 * Time: 19:29
 */

require_once '../database/DBAdapterV2.php';

$_POST = json_decode(file_get_contents('php://input'), true);

if (!empty($_POST)) {
    create();
}

function create()
{
    $db = new DBAdapterV2();
    $params = $_POST['params'];
    $method = $_POST['method'];
    switch ($method) {
        case 'user':
            echoFeedback($db->createUser($params), $method);
            break;
        case 'deck':
            echoFeedback($db->createDeck($params), $method);
            break;
        case 'card':
            echoFeedback($db->createCard($params), $method);
            break;
        case 'examRecord':
            echoFeedback($db->createExamRecord($params), $method);
            break;
    }
}

function echoFeedback($isOperationSuccessful, $method)
{
    $feedback = array();
    if ($isOperationSuccessful) {
        $feedback['method'] = $method;
        $feedback['message'] = 'success';
        $feedback['code'] = 1;
        $feedback['id'] = $isOperationSuccessful;
    } else {
        $feedback['method'] = $method;
        $feedback['message'] = 'failure';
        $feedback['code'] = 0;
        $feedback['id'] = $isOperationSuccessful;
    }
    echo json_encode($feedback);
}