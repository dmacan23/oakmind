<?php
/**
 * Created by PhpStorm.
 * User: David
 * Date: 22.08.14.
 * Time: 21:35
 */

require_once '../database/DatabaseAdapter.php';

$_POST = json_decode(file_get_contents('php://input'), true);

if (!empty($_POST)) {
    if ($_POST["request"] === "catalogues_request")
        echo DatabaseAdapter::readCatalogueData();
}