<?php
/**
 * Created by PhpStorm.
 * User: David
 * Date: 07.09.14.
 * Time: 17:59
 */
require_once '../database/DatabaseAdapter.php';
require_once '../database/DBAdapter.php';

$_POST = json_decode(file_get_contents('php://input'), true);

if (!empty($_POST)) {
    $db = new DBAdapter();
    echo json_encode($db->insert($_POST['table'], $_POST['sets']));
}