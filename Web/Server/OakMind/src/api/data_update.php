<?php
/**
 * Created by PhpStorm.
 * User: David
 * Date: 14.09.14.
 * Time: 23:32
 */

require_once '../database/DBAdapterV2.php';

$_POST = json_decode(file_get_contents('php://input'), true);

if (!empty($_POST)) {
    update();
}

function update()
{
    $db = new DBAdapterV2();
    $params = $_POST['params'];
    $method = $_POST['method'];
    switch ($method) {
        case 'updateUser':
            echoFeedback($db->updateUser($params), $method);
            break;
        case 'updateDeck':
            echoFeedback($db->updateDeck($params), $method);
            break;
        case 'updateCard':
            echoFeedback($db->updateCard($params), $method);
            break;
        case 'deleteDeck':
            echoFeedback($db->deleteDeck($params), $method);
            break;
        case 'deleteCard':
            echoFeedback($db->deleteCard($params), $method);
            break;
        case 'publishDeck':
            echoFeedback($db->publishDeck($params), $method);
            break;
    }
}

function echoFeedback($isOperationSuccessful, $method)
{
    $feedback = array();
    if ($isOperationSuccessful) {
        $feedback['method'] = $method;
        $feedback['message'] = 'success';
        $feedback['code'] = 1;
        $feedback['id'] = $isOperationSuccessful;
    } else {
        $feedback['method'] = $method;
        $feedback['message'] = 'failure';
        $feedback['code'] = 0;
        $feedback['id'] = $isOperationSuccessful;
    }
    echo json_encode($feedback);
}