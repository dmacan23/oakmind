<?php
/**
 * Created by PhpStorm.
 * User: David
 * Date: 13.09.14.
 * Time: 14:07
 */
require_once '../database/DatabaseAdapter.php';
require_once '../file/FileUploader.php';
require_once '../database/DBAdapterV2.php';
//$_FILES = json_decode(file_get_contents('php://input'), true);

if (!empty($_FILES)) {
    upload();
}

function upload()
{
    $myFile = $_FILES["file"];
    $fu = new FileUploader();
    $image = $fu->uploadFileTut($myFile);
    $db = new DBAdapterV2();
    DatabaseAdapter::log("Image: " . $image);
    echoFeedback($db->createMedia($image), 'imageUpload', $image);
}

function echoFeedback($isOperationSuccessful, $method, $fileName)
{
    $feedback = array();
    if ($isOperationSuccessful) {
        $feedback['method'] = $method;
        $feedback['message'] = 'success';
        $feedback['code'] = 1;
        $feedback['id'] = $isOperationSuccessful;
        $filePath = DBAdapterV2::IMAGES_DIR . $fileName;
        $feedback['file_path'] = $filePath;
    } else {
        $feedback['method'] = $method;
        $feedback['message'] = 'failure';
        $feedback['code'] = 0;
        $feedback['id'] = $isOperationSuccessful;
    }
    echo json_encode($feedback);
}