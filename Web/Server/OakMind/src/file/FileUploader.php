<?php

/**
 * Created by PhpStorm.
 * User: David
 * Date: 13.09.14.
 * Time: 13:53
 */
class FileUploader
{
    const FILE_UPLOAD_DIR = "../../img/";

    public function __construct()
    {

    }

    public function uploadFile($myFile)
    {
        $name = preg_replace("/[^A-Z0-9._-]/i", "_", $myFile["name"]);
        $success = move_uploaded_file($myFile["tmp_name"],
            FileUploader::FILE_UPLOAD_DIR . $name);
        chmod(UPLOAD_DIR . $name, 0644);
        return $success;
    }


    public function uploadFileTut($myFile)
    {
        if ($myFile["error"] !== UPLOAD_ERR_OK) {
            echo "<p>An error occurred.</p>";
            exit;
        }

        // ensure a safe filename
        $name = preg_replace("/[^A-Z0-9._-]/i", "_", $myFile["name"]);

        // don't overwrite an existing file
        $i = 0;
        $parts = pathinfo($name);
        while (file_exists(FileUploader::FILE_UPLOAD_DIR . $name)) {
            $i++;
            $name = $parts["filename"] . "-" . $i . "." . $parts["extension"];
        }

        // preserve file from temporary directory
        $success = move_uploaded_file($myFile["tmp_name"],
            FileUploader::FILE_UPLOAD_DIR . $name);
        if (!$success) {
            echo "<p>Unable to save file.</p>";
            exit;
        }

        // set proper permissions on the new file
        chmod(FileUploader::FILE_UPLOAD_DIR . $name, 0644);
        return $name;
    }
}