<?php

/**
 * Created by PhpStorm.
 * User: David
 * Date: 08.09.14.
 * Time: 20:06
 */
require_once "../database/DB.php";
require_once "../database/DatabaseAdapter.php";

class DBAdapterV2
{
    const IMAGES_DIR = "http://ars-pingo.com/oakmind/img/";
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    public function readUserLogin($params)
    {
        $username = $params['username'];
        $password = $params['password'];
        $query = "SELECT u.id, u.username, u.password, u.email, u.registration_date, m.id AS 'm_id', m.location AS 'm_location', m.label AS 'm_label' FROM users u, media m WHERE u.username = \"$username\" AND u.password = \"$password\" AND u.avatar = m.id AND u.deleted = 0 ORDER BY id";
        $result = ($this->db->selectDB($query));
        if ($result == null)
            return json_encode(array(
                'id' => 0
            ));
        $user = array();
        $avatar = array();
        // AVATAR
        $avatar['id'] = $result['m_id'];
        $avatar['location'] = $result['m_location'];
        $avatar['label'] = $result['m_label'];
        // USER
        $user['id'] = $result['id'];
        $user['username'] = $result['username'];
        $user['password'] = $result['password'];
        $user['email'] = $result['email'];
        $user['registration_date'] = $result['registration_date'];
        $user['avatar'] = $avatar;
        return json_encode($user);
    }

    public function readDecks($params = "")
    {
        if ($params != "") {
            $user = $params['id'];
            $where = "d.user = $user";
        } else
            $where = "d.visible = 1";
        $queryDeck = "SELECT d.id, d.label, d.description, d.color, dc.label AS 'dc_label', dc.symbol AS 'dc_symbol' FROM decks d, deck_categories dc WHERE $where AND dc.id = d.category AND d.deleted = 0 ORDER BY id";
        $result = ($this->db->selectDB($queryDeck, false));

        $decks = array();
        if ($result == null)
            return json_encode($decks);
        foreach ($result as $dr) {
            $d = array();
            $dc = array();
            // DECK CATEGORY
            $dc['label'] = $dr['dc_label'];
            $dc['symbol'] = $dr['dc_symbol'];
            // DECK
            $d['id'] = $dr['id'];
            $d['label'] = $dr['label'];
            $d['color'] = $dr['color'];
            $d['description'] = $dr['description'];
            $d['category'] = $dc;
            array_push($decks, $d);
        }
        return json_encode($decks);
    }

    public function readCards($params)
    {
        $deck = $params['id'];
        $query = "SELECT c.id, q.question_title, a.answer_title FROM cards c, questions q, answers a WHERE c.deck = $deck AND q.id = c.question AND a.id = c.answer AND c.deleted = 0 ORDER BY id";
        $result = $this->db->selectDB($query, false);
        $cards = array();
        if ($result == null)
            return json_encode($cards);
        if ($result != null) {
            foreach ($result as $card) {
                $c = array();
                $q = array();
                $a = array();
                $q['title'] = $card['question_title'];
                $a['title'] = $card['answer_title'];
                $c['id'] = $card['id'];
                $c['question'] = $q;
                $c['answer'] = $a;
                array_push($cards, $c);
            }
            return json_encode($cards);
        }
    }

    public function readExamsForDeck($params)
    {
        $deck = $params['id'];
        $query = "SELECT e.id, e.score, e.date_taken, et.label FROM exams e, exam_types et WHERE e.deck = $deck AND e.exam_type = et.id AND e.deleted = 0 ORDER BY id";
        $result = $this->db->selectDB($query, false);
        $exams = array();
        if ($result == null)
            return json_encode($exams);
        foreach ($result as $e) {
            $examType = array();
            $exam = array();
            $examType['label'] = $e['label'];
            $exam['id'] = $e['id'];
            $exam['score'] = $e['score'];
            $exam['date_taken'] = $e['date_taken'];
            $exam['type'] = $examType;
            array_push($exams, $exam);
        }
        return json_encode($exams);
    }

    public function readExamsForUser($params)
    {
        $user = $params['id'];
        $query = "SELECT e.id, e.score, e.date_taken, et.label, d.label AS 'd_label', d.color, dt.symbol FROM exams e, exam_types et, decks d, deck_categories dt WHERE e.deck = d.id AND e.exam_type = et.id AND e.user = $user AND d.category = dt.id AND e.deleted = 0 ORDER BY id";
        $result = $this->db->selectDB($query, false);
        $exams = array();
        if ($result == null)
            return json_encode($exams);
        foreach ($result as $e) {
            $examType = array();
            $exam = array();
            $deck = array();
            $deckCategory = array();
            $deckCategory['symbol'] = $e['symbol'];
            $deck['label'] = $e['d_label'];
            $deck['color'] = $e['color'];
            $deck['category'] = $deckCategory;
            $examType['label'] = $e['label'];
            $exam['id'] = $e['id'];
            $exam['score'] = $e['score'];
            $exam['date_taken'] = $e['date_taken'];
            $exam['type'] = $examType;
            $exam['deck'] = $deck;
            array_push($exams, $exam);
        }
        return json_encode($exams);
    }

    public function readDeckCategories()
    {
        $query = "SELECT id, label, symbol FROM deck_categories WHERE deleted = 0 ORDER BY id";
        $result = $this->db->selectDB($query, false);
        $deckCategories = array();
        if ($result == null)
            return json_encode($deckCategories);
        foreach ($result as $dc) {
            $category = array();
            $category['id'] = $dc['id'];
            $category['label'] = $dc['label'];
            $category['symbol'] = $dc['symbol'];
            array_push($deckCategories, $category);
        }
        return json_encode($deckCategories);
    }

    public function createUser($user)
    {
        $username = $user['username'];
        $password = $user['password'];
        $email = $user['email'];
        $avatar = $user['avatar']['id'];
        $query = "INSERT INTO users (username, password, email, avatar) VALUES (\"$username\", \"$password\", \"$email\", $avatar)";
        return $this->db->queryDB($query);
    }

    public function createDeck($deck)
    {
        $label = $deck['label'];
        $description = $deck['description'];
        $color = $deck['color'];
        $user = $deck['user']['id'];
        $category = $deck['category']['id'];
        $query = "INSERT INTO decks (label, description, user, color, category) VALUES (\"$label\", \"$description\", $user, \"$color\", $category)";
        return $this->db->queryDB($query);
    }

    public function createCard($card)
    {
        $deck = $card['deck']['id'];
        $question = $this->createQuestion($card['question']);
        $answer = $this->createAnswer($card['answer']);
        DatabaseAdapter::log("Question id: " . $question);
        DatabaseAdapter::log("Answer id: " . $answer);
        $query = "INSERT INTO cards (deck, question, answer) VALUES ($deck, $question, $answer)";
        DatabaseAdapter::log("Insert query: " . $query);
        return $this->db->queryDB($query);
    }

    public function createQuestion($question)
    {
        $questionTitle = $question['title'];
        $query = "INSERT INTO questions (question_title) VALUES (\"$questionTitle\")";
        return $this->db->queryDB($query);
    }

    public function createAnswer($answer)
    {
        $answerTitle = $answer['title'];
        $query = "INSERT INTO answers (answer_title) VALUES (\"$answerTitle\")";
        return $this->db->queryDB($query);
    }

    public function createExamRecord($exam)
    {
        $user = $exam['user']['id'];
        $deck = $exam['deck']['id'];
        $score = $exam['score'];
        $examType = $exam['type']['id'];
        $query = "INSERT INTO exams (user, deck, score, exam_type) VALUES ($user, $deck, $score, $examType)";
        return $this->db->queryDB($query);
    }

    public function createMedia($name, $type = 1)
    {
        $label = $name;
        $location = DBAdapterV2::IMAGES_DIR . $name;
        $query = "INSERT INTO media (label, location, type) VALUES (\"$label\", \"$location\", $type)";
        DatabaseAdapter::log("Media query: " . $query);
        return $this->db->queryDB($query);
    }

    public function deleteDeck($deck)
    {
        $id = $deck['id'];
        $query = "UPDATE decks SET deleted = 1 WHERE id = $id";
        $this->db->queryDB($query);
        return $id;
    }

    public function deleteCard($card)
    {
        $id = $card['id'];
        $query = "UPDATE cards SET deleted = 1 WHERE id = $id";
        $this->db->queryDB($query);
        return $id;
    }

    public function updateDeck($deck)
    {
        $id = $deck['id'];
        $label = $deck['label'];
        $description = $deck['description'];
        $color = $deck['color'];
        $category = $deck['category']['id'];
        $query = "UPDATE decks SET label = \"$label\", description = \"$description\", color = \"$color\", category = \"$category\" WHERE id = $id";
        $this->db->queryDB($query);
        return $id;
    }

    public function updateCard($card)
    {
        $this->updateQuestion($card['question']);
        $this->updateAnswer($card['answer']);
    }

    public function updateQuestion($question)
    {
        $id = $question['id'];
        $title = $question['title'];
        $query = "UPDATE questions SET question_title \"$title\" WHERE id = $id";
        $this->db->queryDB($query);
        return $id;
    }

    public function updateAnswer($answer)
    {
        $id = $answer['id'];
        $title = $answer['title'];
        $query = "UPDATE answers SET answer_title \"$title\" WHERE id = $id";
        $this->db->queryDB($query);
        return $id;
    }

    public function updateUser($user)
    {
        $id = $user['id'];
        $username = $user['username'];
        $password = $user['password'];
        $email = $user['email'];
        $avatar = $user['avatar'];
        $query = "UPDATE users SET username = \"$username\", password = \"$password\", email = \"$email\", avatar = $avatar WHERE id = $id";
        $this->db->queryDB($query);
        return $id;
    }

    public function publishDeck($deck)
    {
        $id = $deck['id'];
        $query = "UPDATE decks SET visible = 1 WHERE id = $id";
        $this->db->queryDB($query);
        return $id;
    }

}