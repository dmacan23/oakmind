<?php
/**
 * Created by PhpStorm.
 * User: David
 * Date: 07.09.14.
 * Time: 11:25
 */

require_once "../database/DB.php";
require_once "../database/DatabaseAdapter.php";

class DBAdapter
{

    const KEY_TABLE = 'table';
    const KEY_DISTINCT = 'distinct';
    private $db;
    private $nekaVarijabla;

    function __construct()
    {
        $this->db = new DB();
    }

    function readUsers($where, $distinct = true, $orderBy = "id ASC", $columns = "*")
    {
        $query = "SELECT $columns FROM users";
        return ($this->db->selectDB($query, $distinct));
    }

    function read($table, $where = "", $distinct = false, $orderBy = "id ASC", $columns = "*")
    {
        $query = "SELECT $columns FROM $table";
        if ($where != "")
            $query .= " WHERE $where";
        $query .= " ORDER BY $orderBy";
        DatabaseAdapter::log($query);
        return $this->db->selectDB($query, $distinct);
    }

    function table($JSON)
    {
        return $JSON[DBAdapter::KEY_TABLE];
    }

    function insert($table, $sets)
    {
        $query = "INSERT INTO $table";
        $params = '';
        $values = '';
        $velju = json_encode($sets);
        foreach ($sets as $param => $value) {
            if (!($param == 'id' && $value == 0)) {
                $params .= $param . ', ';
                $values .= "\"$value\"" . ', ';
            }
        }
        $values = substr($values, 0, -2);
        $params = substr($params, 0, -2);
        $query .= "($params) VALUES ($values)";
        return $this->createInsertResponse($this->db->queryDB($query), $table);
    }

    function createInsertResponse($result, $table)
    {
        $code = -1;
        $obj = substr($table, 0, -1);
        $response = array(
            'message' => "$obj creation was unsuccessful",
            'code' => "$code"
        );
        if ($result > 0) {
            $code = 1;
            $response = array(
                'message' => "$obj creation was successful",
                'id' => "$result",
                'code' => "$code"
            );
        }
        return $response;
    }
}