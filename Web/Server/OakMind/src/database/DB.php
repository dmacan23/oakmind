<?php

/**
 * Created by PhpStorm.
 * User: David
 * Date: 16.08.14.
 * Time: 14:01
 */
class DB
{

    private $server;
    private $user;
    private $database;
    private $password;

    /**
     * @param string $server name of the server on which the database is
     * @param string $user username for database access
     * @param string $password password for database access
     * @param string $database database for which the connection is targeted
     */

    function __construct($server = '127.0.0.1', $user = 'arspingo', $password = 'qv5e07XwO6', $database = 'arspingo_oakmind')
    {
        $this->server = $server;
        $this->user = $user;
        $this->password = $password;
        $this->database = $database;
    }

    /**
     * Used for querying the database using SELECT query
     * @param $sqlQuery string SELECT query to be processed
     * @param bool $distinct
     * @return bool|mysqli_result|null
     */
    function selectDB($sqlQuery, $distinct = true)
    {
        $connection = $this->connectDB();

        $result = $connection->query($sqlQuery);

        if (!$result) {
            echo 'No result';
            $result = null;
        }

        while ($row = $result->fetch_assoc()) {
            $rows[] = $row;
        }
        if ($distinct)
            $result = $rows[0];
        else
            $result = $rows;

        $this->closeDB($connection);

        return $result;
    }

    /**
     * Used for connecting to the database
     * @return mysqli connection
     */
    private function connectDB()
    {
        $mysqli = new mysqli($this->server, $this->user, $this->password, $this->database);
        //$mysqli->set_charset("utf8");

        if ($mysqli->connect_errno) {
            Error::showError(Error::DB_CONNECTION_FAIL);
        }
        return $mysqli;
    }

    /**
     * Used for closing the connection to the database
     * @param $connection mysqli connection to the database to be closed
     */
    private function closeDB($connection)
    {
        $connection->close();
    }

    /**
     * Used for querying the database
     * @param $sqlQuery string query to be processed
     * @return bool query success status
     */
    public function queryDB($sqlQuery)
    {
        $connection = $this->connectDB();

        $result = $connection->query($sqlQuery);
        if ($result) {
            $id = $connection->insert_id;
            $this->closeDB($connection);
            return $id;
        }
        $this->closeDB($connection);
        return -1;
    }


}