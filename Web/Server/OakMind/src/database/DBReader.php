<?php

/**
 * Created by PhpStorm.
 * User: David
 * Date: 08.09.14.
 * Time: 19:31
 */
require_once '../database/DatabaseAdapter.php';
require_once '../database/DBAdapter.php';

class DBReader
{
    public static function readUserFull($params, $columns = "*")
    {
        $db = new DBAdapter();
        return json_encode($db->read('users', DBReader::generateWhereParams($params), true, 'id ASC', $columns));
    }

    private static function generateWhereParams($params)
    {
        $keyValueParams = '';
        if ($params == null || $params == "" || $params == "{}")
            return $keyValueParams;
        foreach ($params as $key => $value) {
            $keyValueParams .= "$key = \"$value\" AND ";
        }
        return substr($keyValueParams, 0, -5);
    }
}