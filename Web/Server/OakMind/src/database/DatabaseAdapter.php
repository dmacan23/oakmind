<?php

/**
 * Created by PhpStorm.
 * User: David
 * Date: 16.08.14.
 * Time: 14:51
 */
require_once "../database/Database.php";

class DatabaseAdapter
{
    public static function readUser($username, $password)
    {
        $db = new Database();
        $query = "SELECT * FROM users WHERE username = \"$username\" AND password = \"$password\"";
        //return json_encode(($db->queryDB($query)));
        $user = mysqli_fetch_assoc($db->selectDB($query));
        $queryAvatar = "SELECT * FROM media WHERE id = " . $user['avatar'];
        $avatar = mysqli_fetch_array($db->selectDB($queryAvatar));
        $queryMediaType = "SELECT * FROM media_types WHERE id = " . $avatar['type'];
        $mediaType = mysqli_fetch_array($db->selectDB($queryMediaType));
        $avatar['type'] = $mediaType;
        $user['avatar'] = $avatar;
        return json_encode($user);
    }

    public static function createUser($username, $password, $email, $avatar)
    {
        $avatar = $avatar['id'];
        $query = "INSERT INTO users ";
        $query .= "(username, email, password, avatar) ";
        $query .= "VALUES ";
        $query .= "(\"" . $username . "\", \"" . $email . "\", \"" . $password . "\", " . $avatar . ");";
        $db = new Database();
        return $db->queryDB($query);
    }

    public static function readCatalogueData()
    {
        $mediaTypes = json_encode(DatabaseAdapter::readCatalogueTable("media_types"));
        $examTypes = json_encode(DatabaseAdapter::readCatalogueTable("exam_types"));
        $deckCategories = json_encode(DatabaseAdapter::readCatalogueTable("deck_categories"));
        $retVal = "{\"media_types\" : $mediaTypes, \"exam_types\" : $examTypes, \"deck_categories\" : $deckCategories}";
        return ($retVal);
    }

    public static function readCatalogueTable($catalogueTable)
    {
        $query = "SELECT * FROM " . $catalogueTable;
        $db = new Database();
        $result = $db->selectDB($query);
        while ($row = mysqli_fetch_array($result)) {
            $json[] = $row;
        }
        return ($json);
    }

    public static function createCard($card)
    {
        $db = new Database();
        $question = $card['question']['title'];
        $answer = $card['answer']['title'];
        $deck = $card['deck']['id'];


        $queryQuestion = "INSERT INTO questions (title) VALUES (\"$question\");";
        $queryAnswer = "INSERT INTO answers (title) VALUES(\"$answer\");";
        $questionId = $db->queryDB($queryQuestion);
        $answerId = $db->queryDB($queryAnswer);
        $queryCard = "INSERT INTO cards(question, answer, deck) VALUES ($questionId, $answerId, $deck)";
        return $db->queryDB($queryCard);
    }

    public static function createDeck($deck)
    {
        $db = new Database();
        $label = $deck['label'];
        $color = $deck['color'];
        $user = $deck['user'];
        $user = $user['id'];
        $category = $deck['category']['id'];
        $description = $deck['description'];
        $query = "INSERT INTO decks (label, color, user, category, description) VALUES (\"$label\", \"$color\", $user, $category, \"$description\");";
        $id = $db->queryDB($query);
        return $id;
    }

    public static function readMediaById($id)
    {
        $db = new Database();
        $query = "SELECT * FROM media WHERE id = $id";
        $result = $db->selectDB($query);
        while ($row = mysqli_fetch_array($result)) {
            $json[] = $row;
        }
        $media = $json[0];
        $media['type'] = DatabaseAdapter::readMediaCategoryById($media['type']);
        return ($media);
    }

    public static function readMediaCategoryById($id)
    {
        $db = new Database();
        $query = "SELECT * FROM media_types WHERE id = $id";
        $result = $db->selectDB($query);
        while ($row = mysqli_fetch_array($result)) {
            $json[] = $row;
        }
        return ($json[0]);
    }

    /* public static function readUserById($id)
     {
         $db = new Database();
         $query = "SELECT * FROM users WHERE id = $id";
         $result = $db->selectDB($query);
         while ($row = mysqli_fetch_array($result)) {
             $json[] = $row;
         }
         $user = $json[0];
         DatabaseAdapter::log("User avatar: " . $user['avatar']);
         $user['avatar'] = DatabaseAdapter::readMediaById($user['avatar']);
         DatabaseAdapter::log("User avatar2: " . $user['avatar']);
         return ($json[0]);
     }*/

    public static function readDecksFromUser($user)
    {
        $db = new Database();
        $query = "SELECT * FROM decks WHERE user = $user";
        $result = $db->selectDB($query);
        while ($row = mysqli_fetch_array($result)) {
            $row['user'] = DatabaseAdapter::readUserById($row['user']);
            $row['category'] = DatabaseAdapter::readDeckCategory($row['category']);
            $json[] = $row;
        }
        return json_encode(array('decks' => $json));
    }

    public static function readPublicDecks(){
        $db = new Database();
        $query = "SELECT * FROM decks WHERE visible = 1";
        $result = $db->selectDB($query);
        while ($row = mysqli_fetch_array($result)) {
            $row['user'] = DatabaseAdapter::readUserById($row['user']);
            $row['category'] = DatabaseAdapter::readDeckCategory($row['category']);
            $json[] = $row;
        }
        return json_encode(array('decks' => $json));
    }

    public static function readUserById($id)
    {
        $db = new Database();
        $query = "SELECT * FROM users WHERE id = $id";
        $user = mysqli_fetch_assoc($db->selectDB($query));
        $queryAvatar = "SELECT * FROM media WHERE id = " . $user['avatar'];
        $avatar = mysqli_fetch_array($db->selectDB($queryAvatar));
        $queryMediaType = "SELECT * FROM media_types WHERE id = " . $avatar['type'];
        $mediaType = mysqli_fetch_array($db->selectDB($queryMediaType));
        $avatar['type'] = $mediaType;
        $user['avatar'] = $avatar;
        return $user;
    }

    public static function readDeckCategory($id)
    {
        $db = new Database();
        $query = "SELECT * FROM deck_categories WHERE id = $id;";
        $result = $db->selectDB($query);
        while ($row = mysqli_fetch_array($result)) {
            $json[] = $row;
        }
        return ($json[0]);
    }

    public static function deleteDeck($deck)
    {
        DatabaseAdapter::deleteCardsFromDeck($deck);
        $db = new Database();
        $query = "DELETE FROM decks WHERE id = $deck";
        $db->queryDB($query);
    }

    public static function deleteCardsFromDeck($deck)
    {
        $db = new Database();
        $query = "SELECT * FROM cards WHERE deck = $deck";
        $result = $db->selectDB($query);
        while ($row = mysqli_fetch_array($result)) {
            DatabaseAdapter::deleteCard($row);
        }
    }

    public static function deleteCard($card)
    {
        DatabaseAdapter::deleteAnswer($card['answer']);
        DatabaseAdapter::deleteQuestion($card['question']);
        $query = "DELETE FROM cards WHERE id = " . $card['id'];
        $db = new Database();
        return $db->queryDB($query);
    }

    public static function deleteAnswer($answer)
    {
        $query = "DELETE FROM answers WHERE id = $answer";
        $db = new Database();
        return $db->queryDB($query);
    }

    public static function deleteQuestion($question)
    {
        $query = "DELETE FROM questions WHERE id = $question";
        $db = new Database();
        return $db->queryDB($query);
    }

    public static function readCardsFromDeck($deck)
    {
        $db = new Database();
        $query = "SELECT * FROM cards WHERE deck = $deck";
        $result = $db->selectDB($query);
        while ($row = mysqli_fetch_array($result)) {
            $row['question'] = DatabaseAdapter::readQuestion($row['question']);
            $row['answer'] = DatabaseAdapter::readAnswer($row['answer']);
            $row['deck'] = DatabaseAdapter::readDeck($row['deck']);
            $json[] = $row;
        }
        return json_encode(array('cards' => $json));
    }

    public static function readQuestion($id)
    {
        $db = new Database();
        $query = "SELECT * FROM questions WHERE id = $id";
        $question = mysqli_fetch_assoc($db->selectDB($query));
        return $question;
    }

    public static function readAnswer($id)
    {
        $db = new Database();
        $query = "SELECT * FROM answers WHERE id = $id";
        $answer = mysqli_fetch_assoc($db->selectDB($query));
        return $answer;
    }

    public static function readDeck($id)
    {
        $db = new Database();
        $query = "SELECT * FROM decks d, deck_categories dc WHERE d.id = $id AND d.category = dc.id;";
        $result = $db->selectDB($query);
        while ($row = mysqli_fetch_array($result)) {
            $json[] = $row;
        }
        $deck = $json[0];
        $deckCategory = DatabaseAdapter::readDeckCategory($deck['category']);
        $deckUser = DatabaseAdapter::readUserById($deck['user']);
        $retVal = array(
            'id' => $deck['id'],
            'user' => $deckUser,
            'category' => $deckCategory,
            'color' => $deck['color'],
            'label' => $deck['label'],
            'description' => $deck['description']
        );
        return $retVal;
    }

    public static function getRowId($table, $where)
    {
        $query = "SELECT id FROM $table WHERE $where LIMIT 1";
        DatabaseAdapter::log("Query: $query");
        $db = new Database();
        $result = mysqli_fetch_assoc($db->selectDB($query));
        DatabaseAdapter::log('id: ' . $result['id']);
    }

    public static function log($message, $user = 0)
    {
        $db = new Database();
        $query = "INSERT INTO log (action, user) VALUES (\"" . $message . "\", " . $user . ")";
        $db->queryDB($query);
        return $query;
    }

    /*
        public static function readUser($username, $password)
        {
            $db = new Database();
            $query = "SELECT * FROM users WHERE username = \"$username\" AND password = \"$password\"";
            //return json_encode(($db->queryDB($query)));
            $user = mysqli_fetch_assoc($db->selectDB($query));
            $queryAvatar = "SELECT * FROM media WHERE id = " . $user['avatar'];
            $avatar = mysqli_fetch_array($db->selectDB($queryAvatar));
            $queryMediaType = "SELECT * FROM media_types WHERE id = " . $avatar['type'];
            $mediaType = mysqli_fetch_array($db->selectDB($queryMediaType));
            $avatar['type'] = $mediaType;
            $user['avatar'] = $avatar;
            return json_encode($user);
        }*/
}